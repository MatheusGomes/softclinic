<?php

ob_start();
session_start();

require __DIR__ . "/vendor/autoload.php";

//$clientes = new Source\Models\Clients();

// Recuperar o primeiro registro do banco de dados
//var_dump($clientes->find()->fetch());

// Recuperar todos os registros do banco de dados
//var_dump($clientes->find()->fetch(true));

// Recuperar por id
//var_dump($clientes->findById(2));

//Recuperar usando limit
//var_dump($clientes->find()->limit(2)->fetch(true));

//Recuperar usando limit e offset
//var_dump($clientes->find()->limit(2)->offset(2)->fetch(true));

//Recuperar usando order
//var_dump($clientes->find()->order("nmCliente ASC")->fetch(true));

//Recuperar usando limit, offset e order
//var_dump($clientes->find()->limit(2)->offset(2)->order("nmCliente ASC")->fetch(true));
//$cliente = new ParticipantModel();
//$cliente->cdCliente = 6;

//$user = (new UserModel())->findById(12);
//$user->add($cliente);
//var_dump($user->save(), $user->fail());

ob_end_flush();