$(function () {
    $('.select-single').select2({
        placeholder: "Selecione uma opção",
        language: "pt-br"
    });

    $('.select-multiple').select2({
        placeholder: "Selecione as opções",
        language: "pt-br"
    });

    if($("#sexoAnimal").length) {
        alteraFormSexo($("#sexoAnimal")[0].value);
    }

    $(document).on("change", "#sexoAnimal", function (e) {
        alteraFormSexo(this.value);
    });

    function alteraFormSexo(sexo) {
        if (sexo === "macho") {
            $('.anamnese .femea').fadeOut();
            $('.anamnese .macho').fadeIn();
        }

        if (sexo === "femea") {
            $('.anamnese .macho').fadeOut();
            $('.anamnese .femea').fadeIn();
        }
    }

    $('[data-toggle="tooltip"]').tooltip();


    if($("#js-form").length) {
        //ajax form
        $("#js-form").submit(function (e) {
            e.preventDefault();

            var form = $(this);

            form.ajaxSubmit({
                url: form.attr("action"),
                type: "POST",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (response) {
                    //redirect
                    if (response.redirect) {
                        window.location.href = response.redirect;
                    }

                    //message
                    if (response.message) {
                        scrollDown();
                        $('#chat ul').append(`
                            <li class="chat-item chat-from">
                            <div class="chat-body">
                                <div class="chat-header">
                                    <span class="chat-name">${response.message.author}</span>
                                    <span class="chat-datetime" data-datetime="${response.message.dataEnvio}">${response.message.dataEnvio}</span>
                                </div>
                                <div class="chat-message">${response.message.conteudo}</div>
                            </div>
                        </li>`);
                        $("li.new:last").slideDown('slow', 'easeOutBounce');
                    } else {
                        console.log(response.error);
                    }
                },
                complete: function () {
                    if (form.data("reset") === true) {
                        form.trigger("reset");
                    }
                }
            });
        });
    }
    if($("#chat").length) {
        scrollDown();
        setInterval(function () {
            let message = window.location.pathname.split("/")[4];
            let datetime = $(".chat-datetime:last").data("datetime");
            let date = datetime.split(" ")[0];

            $.get( "https://softclinic.pc/panel/chats/messages/" + message + "&datetime=" + date, function( data ) {
                data.responses.forEach(function(dataItem) {
                    if (new Date(dataItem.dtenvio) > new Date(datetime)) {
                        $('#chat ul').append(`
                            <li class="chat-item ${(dataItem.cdusuario_remetente === data.user_logged) ? "chat-from" : "chat-self"}">
                            <div class="chat-body">
                                <div class="chat-header">
                                    <span class="chat-name">${(data.cdusuario_remetente === data.user_logged) ? data.nomedestinatario : data.nomeremetente}</span>
                                    <span class="chat-datetime" data-datetime="${dataItem.dtenvio}">${dataItem.dtenvio}</span>
                                </div>
                                <div class="chat-message">${dataItem.conteudo}</div>
                            </div>
                        </li>`);
                        $("li.new:last").slideDown('slow', 'easeOutBounce');

                        if (data.cdusuario_remetente === data.user_logged) {
                            $(".new-messages").fadeIn();
                        }
                    }
                });
            }, 'json');
        }, 1000);
    }

    $(document).on('click', '.j_delete', function (e) {
        e.preventDefault();
        e.stopPropagation();

        let route = $(this).attr('href');

        $('body').addClass('modal-show');
        $("body").append(`<div class="modal-delete">
                            <div class="modal-delete-content">
                                <div class="icon d-block">
                                    <i class="fas fa-trash-alt"></i>
                                </div>
                                <h2 class="header">Deletar item</h2>
                                <div class="description"> Voce tem certeza que quer deletar este item? esta açao nao podera ser desfeita!</div>
                                <div class="actions">
                                    <button class="btn btn-sm btn-danger cancel mr-2">Cancelar</button>
                                    <button class="btn btn-sm btn-success approved">Continuar</button>
                                </div>
                            </div>
                           </div>'`).fadeIn('slow');

        $(document).on('click', '.modal-delete .cancel', function (e) {
            e.preventDefault();
            e.stopPropagation();

            $('.modal-delete').remove();
            $('body').removeClass('modal-show');
        });

        $(document).on('click', '.modal-delete .approved', function (e) {
            e.preventDefault();
            e.stopPropagation();

            $('.modal-delete').remove();
            $('body').removeClass('modal-show');

            window.location.href = route;
        });

        return false;
    });

    function scrollDown() {
        var wrapUl = $("#chat ul");
        $('#chat').animate({
            scrollTop: wrapUl.height()
        }, 500);
    }

    function getRandom(max) {
        return Math.floor(Math.random() * max + 1)
    }
});