<?php

/**
 * TimeZone America/Sao_Paulo
 */
ini_set("date.timezone", "America/Sao_Paulo");

/**
 * Site configuration
 */
define("SITE", [
    "name" => "SoftClinic - Software para gestão de clínicas veterinárias",
    "desc" => "Gerencie a sua clínica de forma rápida e fácil!",
    "domain" => "softclinic.dev",
    "locale" => "pt_BR",
    "root" => "https://softclinic.pc"
]);

/**
 * Database configuration
 */
define("DATABASE", [
    "driver" => "pgsql",
    "host" => "localhost",
    "port" => 5432,
    "dbname" => "softclinic",
    "username" => "postgres",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

/**
 * Email configuration
 */
define("MAIL", [
    "host" => "smtp.gmail.com",
    "port" => "587",
    "username" => "softclinicpet@gmail.com",
    "passwd" => "softclinic@fap2020",
    "from_name" => SITE["name"],
    "from_email" => "softclinicpet@gmail.com",
    "secure" => "tls",
    "charset" => "utf-8",
    "expireToken" => "+5 minutes"
]);

/**
 * Encrypt configuration
 */

define("ENCRYPT", [
    "keySecurity" => "fvG$9oP)1",
]);