<?php


namespace Source\Controllers;


use Dompdf\Dompdf;
use Source\Core\Controller;
use Source\Core\View;
use Source\Models\AnamneseModel;
use Source\Models\AnimalModel;
use Source\Models\ParticipantModel;

/**
 * Class AnamneseController
 * @package Source\Controllers
 */
class AnamneseController extends Controller
{
    /**
     * PanelController constructor.
     */
    public function __construct()
    {
        nonLogged();
        parent::__construct();
    }

    public function index()
    {
        hasPermission('list-anamneses');

        $anamneses = (new AnamneseModel())->find(null,  null, "cdanamnese, cdparticipante, cdanimal, dtcadastro")->fetch(true);
        $listAnamnese = [];
        if ($anamneses) {
            /** @var $anamneseItem AnamneseModel */
            foreach ($anamneses as $anamneseItem) {
                $listAnamnese[] = (object) array_merge(
                    (array) $anamneseItem->data(),
                    ["nmparticipante" => $anamneseItem->participant()->data()->nmparticipante],
                    ["nmanimal" => $anamneseItem->animal()->data()->nmanimal],
                );
            }
        }

        View::make("anamnese.index", ["anamneses" => $listAnamnese]);
    }

    public function register($request)
    {
        hasPermission('create-anamneses');

        $participants = (new ParticipantModel())->find("cdtipoparticipante = :typeParticipant", "typeParticipant=1")->fetch(true);
        $listParticipants = [];
        if($participants) {
            foreach ($participants as $participantItem) {
                $listParticipants[] = $participantItem->data();
            }
        }

        if ($request->post) {
            $animal = new AnimalModel();

            if ($this->saveAnimal($request, $animal)) {
                $anamnese = new AnamneseModel();
                if ($this->saveAnamnese($request, $anamnese, $animal)) {
                    setFlash("success", "Anamnese criada com sucesso!");
                } else {
                    setFlash("danger", "Ocorreu um erro ao tentar salvar, contate o administrador. Error: {$anamnese->fail()->getMessage()}");
                }
            } else {
                setFlash("danger", "Ocorreu um erro ao tentar salvar, contate o administrador. Error: {$animal->fail()->getMessage()}");
            }
        }

        View::make("anamnese.register", ["participants" => $listParticipants]);
    }

    public function edit($anamneseSelected, $request)
    {
        hasPermission('edit-anamneses');

        /** @var $anamneseEdit AnamneseModel*/
        $anamneseEdit = (new AnamneseModel())->findById($anamneseSelected->anamneseId);
        if (!$anamneseEdit) {
            setFlash("warning", "A anamnese que você tentou editar não existe!");
            redirect("/panel/anamneses");
            exit();
        }

        $anamneseEdit->informacoes = json_decode($anamneseEdit->informacoes);
        $participants = $anamneseEdit->participant()->data();
        $animal = $anamneseEdit->animal()->data();

        if ($request->post) {
            /** @var $animal AnimalModel */
            $animal = (new AnimalModel())->findById($animal->cdanimal);

            if ($this->saveAnimal($request, $animal)) {
                /** @var $anamnese AnamneseModel */
                $anamnese = (new AnamneseModel())->findById($anamneseSelected->anamneseId);

                if ($this->saveAnamnese($request, $anamnese, $animal)) {
                    setFlash("success", "Anamnese editada com sucesso!");
                    redirect("/panel/anamneses/edit/{$anamneseSelected->anamneseId}");
                } else {
                    setFlash("danger", "Ocorreu um erro ao tentar salvar, contate o administrador. Error: {$anamnese->fail()->getMessage()}");
                    redirect("/panel/anamneses/edit/{$anamneseSelected->anamneseId}");
                }
            } else {
                setFlash("danger", "Ocorreu um erro ao tentar salvar, contate o administrador. Error: {$animal->fail()->getMessage()}");
                redirect("/panel/anamneses/edit/{$anamneseSelected->anamneseId}");
            }
        }

        View::make("anamnese.register", ["anamnese"=> $anamneseEdit->data(), "participant" => $participants, "animal" => $animal]);
    }

    public function print($anamneseSelected)
    {
        /** @var $anamnese AnamneseModel*/
        $anamnese = (new AnamneseModel())->findById($anamneseSelected->anamneseId);
        if (!$anamnese) {
            setFlash("warning", "A anamnese que você tentou imprimir não existe!");
            redirect("/panel/anamneses");
            exit();
        }

        $anamnese->informacoes = json_decode($anamnese->informacoes);
        $participant = $anamnese->participant()->data();
        $animal = $anamnese->animal()->data();

        View::make("reports.anamnese",
            ["anamnese" => $anamnese->data(),
            "participant" => $participant,
            "animal" => $animal
            ]);

        $dompdf = new Dompdf();
        $dompdf->loadHtml(ob_get_clean());

        $dompdf->setPaper("A4");

        $dompdf->render();
        $dompdf->stream("anamnese-{$animal->nmanimal}.pdf");
        die();
    }

    protected function saveAnamnese(object $request, AnamneseModel $anamnese, AnimalModel $animal): bool
    {
        // Geral
        $anamnese->cdanimal = $animal->cdanimal;
        $anamnese->cdparticipante = $request->post->participante ?? $anamnese->cdparticipante;
        $anamnese->informacoes = json_encode([
            "queixaprincipal" => $request->post->queixaPrincipal ?? "",
            "tempoevolucao" => $request->post->tempoEvolucao ?? "",
            "tipoevolucao" => $request->post->tipoEvolucao ?? "",
            "tipotratamento" => $request->post->tipoTratamento ?? "",
            "alimentacao" => $request->post->alimentacao ?? "",
            "vacinacao" => $request->post->vacinacao ?? "",
            "vermifugacao" => $request->post->vermifugacao ?? "",
            "ambiente" => $request->post->ambiente ?? "",
            "acessorua" => $request->post->acessoRua ?? "",
            "doencasanteriores" => $request->post->doencasAnteriores ?? "",
            // Digestório
            "ingestaoalimento" => $request->post->ingestaoAlimento ?? "",
            "ingestaoagua" => $request->post->ingestaoAgua ?? "",
            "defecacao" => $request->post->defecacao ?? "",
            "abdomen" => $request->post->abdomen ?? "",
            "observacaodigestorio" => $request->post->observacaoDigestorio ?? "",
            // Cardio respiratorio
            "cansacofacil" => $request->post->cansacoFacil ?? "",
            "apresentacianose" => $request->post->apresentaCianose ?? "",
            "tosseanimal" => $request->post->tosseAnimal ?? "",
            "tipoexpectoracao" => $request->post->tipoExpectoracao ?? "",
            "apresentaespirros" => $request->post->apresentaEspirros ?? "",
            "secrecaonasal" => $request->post->secrecaoNasal ?? "",
            "inchacoanimal" => $request->post->inchacoAnimal ?? "",
            "animalfraco" => $request->post->animalFraco ?? "",
            // Urinário
            "animalurinando" => $request->post->animalUrinando ?? "",
            "urinacomformigas" => $request->post->urinaComFormigas ?? "",
            "frequenciaurina" => $request->post->frequenciaUrina ?? "",
            "coloracaourina" => $request->post->coloracaoUrina ?? "",
            "odorurina" => $request->post->odorUrina ?? "",
            "dorurina" => $request->post->dorUrina ?? "",
            "tipourina" => $request->post->tipoUrina ?? "",
            // genital femea
            "animalcastrado" => $request->post->animalCastrado ?? "",
            "japariu" => $request->post->jaPariu ?? "",
            "secrecaovaginal" => $request->post->secrecaoVaginal ?? "",
            "presencacio" => $request->post->presencaCio ?? "",
            "anticoncepcional" => $request->post->anticoncepcional ?? "",
            "sangramentovaginal" => $request->post->sangramentoVaginal ?? "",
            "nodulomamario" => $request->post->noduloMamario ?? "",
            // Genital macho
            "testiculos" => $request->post->testiculos ?? "",
            "secrecaopeniana" => $request->post->secrecaoPeniana ?? "",
            "sangramentopeniano" => $request->post->sangramentoPeniano ?? "",
            // Nervoso
            "comportamento" => $request->post->comportamento ?? "",
            "convulsoes" => $request->post->convulsoes ?? "",
            "dificuldadesAndar" => $request->post->dificuldadesAndar ?? "",
            "andaemcirculos" => $request->post->andaEmCirculos ?? "",
            "inclinacaocabeca" => $request->post->inclinacaoCabeca ?? "",
            "apresentatropecos" => $request->post->apresentaTropecos ?? "",
            "atendechamados" => $request->post->atendeChamados ?? "",
            "ficadeitado" => $request->post->ficaDeitado ?? "",
            "respondeestimulos" => $request->post->respondeEstimulos ?? "",
            // Esqueletico
            "animalmancando" => $request->post->animalMancando ?? "",
            "traumasrecentes" => $request->post->traumasRecentes ?? "Não informado",
            "sentedores" => $request->post->senteDores ?? "",
            "animalandando" => $request->post->animalAndando ?? "",
            // Dermatologico
            "animalseconcando" => $request->post->animalSeConcando ?? "",
            "maneiosdecabeca" => $request->post->maneiosDeCabeca ?? "",
            "quedadepelos" => $request->post->quedaDePelos ?? "",
            "tevecarrapatos" => $request->post->teveCarrapatos ?? "",
        ]);

        if ($anamnese->save()) {
            return true;
        }

        return false;
    }

    protected function saveAnimal(object $request, AnimalModel $animal): bool
    {
        $animal->sexo = $request->post->sexoAnimal ?? "";
        $animal->nmanimal = $request->post->nomeAnimal ?? "";
        $animal->especie = $request->post->especieAnimal ?? "";
        $animal->raca = $request->post->racaAnimal ?? "";
        $animal->idade = $request->post->idadeAnimal ?? "";
        $animal->peso = $request->post->pesoAnimal ?? "";

        if ($animal->save()) {
            return true;
        }

        return false;
    }

    public function delete($anamneseId)
    {
        hasPermission('delete-anamneses');

        $anamnese = (new AnamneseModel())->findById($anamneseId->anamneseId);

        if (!$anamnese) {
            setFlash("warning", "A Anamnese que você tentou deletar não existe!");
            redirect("/panel/anamneses");
            exit();
        }

        if ($anamnese->destroy()) {
            setFlash("success", "Anamnese deletada com sucesso!");
            redirect("/panel/anamneses");
            exit();
        } else {
            setFlash("danger", "Ocorreu um erro ao tentar deletar. Error: {$anamnese->fail()->getMessage()}");
        }
    }

}