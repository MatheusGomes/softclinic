<?php


namespace Source\Controllers;


use Source\Core\Controller;
use Source\Core\Email;
use Source\Core\Encrypt;
use Source\Core\Session;
use Source\Core\View;
use Source\Models\CityModel;
use Source\Models\ParticipantModel;
use Source\Models\ResetPasswordModel;
use Source\Models\SexModel;
use Source\Models\UserModel;

/**
 * Class AuthController
 * @package Source\Controllers
 */
class AuthController extends Controller
{
    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * login controller
     * @param $request
     */
    public function login($request)
    {
        if (Session::has("auth")) {
            redirect("/panel");
            exit();
        }

        if ($request->post) {
            if (required(['email', 'passwd'], (array)$request->post)) {
                $auth = (new UserModel)->auth($request->post->email, Encrypt::hash($request->post->passwd));

                if ($auth) {
                    Session::set("auth", [
                        "cdusuario" => $auth->cdusuario,
                        "nome" => $auth->participant()->nmparticipante ?? "",
                        "email" => $auth->emailUsuario,
                        "permissions" => $auth->listPermissions()
                    ]);

                    redirect("/panel");
                }
                setFlash("warning", "E-mail ou senha inválidos!");
            } else {
                setFlash("warning", "Preencha todos os campos!");
            }
        }

        View::make("auth.login");
    }

    /**
     * register controller
     * @param $request
     */
    public function register($request)
    {
        if (Session::has("auth")) {
            redirect("/panel");
            exit();
        }

        if (Session::has("userData")) {
            setFlash("success", "Olá, continue o seu cadastro!");
            redirect("/complete-register");
            exit();
        }

        $step = 1;

        if ($request->post) {
            $required = ["email", "senha", "confirmaSenha"];

            if (!required($required, (array) $request->post)) {
                setFlash("warning", "Exite campos em brancos!");
                redirect("/register");
                exit();
            }

            if ($request->post->senha === $request->post->confirmaSenha) {
                // Cadastra usuario
                $user = new UserModel();
                $user->emailusuario = $request->post->email;
                $user->senhausuario = Encrypt::hash($request->post->senha);

                if ((!$user->userHasParticipant()->cdparticipante) && ($user->userHasParticipant()->senhausuario === Encrypt::hash($request->post->senha))) {
                    Session::set("userData", [$user->userHasParticipant()->cdusuario, $user->emailusuario]);
                    setFlash("success", "Olá, complete o seu cadastro");
                    redirect("/complete-register");
                }


                if (!$user->unique("emailusuario")) {
                    setFlash("warning", "Já exite um usuário com esse <strong>Email</strong>. <br /> Caso esqueceu a senha, clique <a href='" . site() . "/recovery'>aqui para recuperar</a>!");
                    redirect("/register");
                    exit();
                }

                if ($user->save()) {
                    Session::set("userData", [$user->cdusuario, $user->emailusuario]);
                    setFlash("success", "Usuário cadastrado, preencha seus dados pessoais!");
                    redirect("/complete-register");
                    exit();
                }

                setFlash("warning", "E-mail já cadastrado!");
            } else {
                setFlash("warning", "As senhas não conferem!");
            }
        }

        View::make("auth.register", ["step" => $step]);
    }

    /**
     * complete register controller
     * @param $request
     */
    public function CompleteRegister($request)
    {
        if (Session::has("auth")) {
            redirect("/panel");
            exit();
        }

        if (!Session::has("userData")) {
            redirect("/register");
            exit();
        }

        $step = 2;
        $cityParticipant = (new CityModel())->find()->fetch(true);
        $sexParticipant = (new SexModel())->find()->fetch(true);

        foreach ($cityParticipant as $cityItem) {
            $listCity[] = $cityItem->data();
        }

        foreach ($sexParticipant as $sexParticipantItem) {
            $listSex[] = $sexParticipantItem->data();
        }

        if ($request->post) {
            $required = ["nome", "cep", "rua", "bairro", "cpf", "telefone"];

            if (!required($required, (array)$request->post)) {
                setFlash("warning", "Exite campos em brancos!");
                redirect("/complete-register");
                exit();
            }

            // create participant
            $participant = new ParticipantModel();
            $participant->nmparticipante = $request->post->nome;
            $participant->numcep = $request->post->cep;
            $participant->nmlogradouro = $request->post->rua;
            $participant->nmbairro = $request->post->bairro;
            $participant->cdsexo = $request->post->sexo;
            $participant->cdcidade = $request->post->cidade;
            $participant->cpfparticipante = $request->post->cpf;
            $participant->numcelular = $request->post->telefone;
            $participant->email = Session::get("userData")[1];

            if (!$participant->unique("cpfparticipante")) {
                setFlash("warning", "Já exite um participante com esse <strong>CPF</strong>!");
                redirect("/complete-register");
                exit();
            }

            if (!$participant->unique("email")) {
                setFlash("warning", "Já exite um participante com esse <strong>Email</strong>!");
                redirect("/complete-register");
                exit();
            }

            if ($participant->save()) {
                /* @var $user UserModel */
                $user = (new UserModel())->findById(Session::get("userData")[0]);
                $user->add($participant);
                $user->save();

                Session::delete('userData');
                setFlash("success", "Cadastro conlcuido, faça o login abaixo!");
                redirect("/");
                exit();
            } else {
                setFlash("danger", "Ocorreu um erro ao tentar salvar. {$participant->fail()->getMessage()}");
            }
        }

        View::make("auth.register", ["step" => $step, "listCity" => $listCity, "listSex" => $listSex]);
    }

    /**
     * logout controller
     */
    public function logout()
    {
        if (Session::has("auth")) {
            Session::delete("auth");
            setFlash("success", "Você saiu do sistema, volte quando quiser!");
        }

        redirect("/");
    }

    /**
     * recovery method
     * @param $request
     * @throws \Exception
     */
    public function recovery($request)
    {
        if ($request->post) {
            $required = ["email"];

            if (!required($required, (array)$request->post)) {
                setFlash("warning", "Campo email em branco!");
                redirect("/recovery");
                exit();
            }

            $user = (new UserModel())->find("emailusuario = :mail", "mail={$request->post->email}", "emailusuario")->fetch();

            if (!$user) {
                setFlash("warning", "Email não cadastrado no sistema!");
                redirect("/recovery");
                exit();
            }

            $expireOn = (new \DateTime(" " . MAIL["expireToken"]))->format("Y-m-d H:i:s");
            $token = Encrypt::hash($expireOn . $user->emailusuario);

            $resetPassword = new ResetPasswordModel();
            $resetPassword->email = $user->emailusuario;
            $resetPassword->token = $token;
            $resetPassword->expiracao = $expireOn;

            if ($resetPassword->save()) {
                $url = site() . "/resetpassword/{$token}";
                $mail = new Email();
                $mail->add(
                    "Recuperação de senha",
                    View::make("emails.reset-pass", ["email" => $user->emailusuario, "url" => $url], true),
                    $user->emailusuario,
                    $user->emailusuario);

                if ($mail->send()) {
                    setFlash("success", "Enviamos um email para você com as instruções de recuperação de senha.");
                } else {
                    setFlash("success", "Ocorreu um erro ao enviar o email. <br /> Error: {$mail->error()}");
                }
            } else {
                setFlash("danger", "Ocorreu um erro ao tentar resetar sua senha. <br /> Error: {$resetPassword->fail()->getMessage()}");
            }
        }

        View::make("auth.recovery");
    }

    /**
     * @param $token
     * @param $request
     * @throws \Exception
     */
    public function resetPassword($token, $request)
    {
        if (empty($token->token)) {
            setFlash("warning", "Token expirado, solicite novamente!");
            redirect("/");
            exit();
        }

        /** @var  $getUserReset ResetPasswordModel */
        $getUserReset = (new ResetPasswordModel())->find("token = :utoken AND ativo = :uativo", "utoken={$token->token}&uativo=1")->fetch();

        if (!$getUserReset) {
            setFlash("warning", "Token expirado, solicite novamente!");
            redirect("/");
            exit();
        }

        $dateNow = new \DateTime();
        $dateExpire = new \DateTime($getUserReset->expiracao);

        if ($dateExpire < $dateNow) {
            $getUserReset->ativo = 2;
            $getUserReset->save();
            setFlash("warning", "Token expirado, solicite novamente!");

            redirect("/");
        }

        if ($request->post) {
            $required = ["senha", "confirmaSenha"];

            if (!required($required, (array)$request->post)) {
                setFlash("warning", "Preencha todos os campos!");
                redirect("/resetpassword/{$token->token}");
                exit();
            }

            if ($request->post->senha !== $request->post->confirmaSenha) {
                setFlash("warning", "As senhas não conferem!");
                redirect("/resetpassword/{$token->token}");
                exit();
            }

            /** @var $user UserModel */
            $user = (new UserModel())->find("emailusuario = :mail", "mail={$getUserReset->email}")->fetch();
            $user->senhausuario = Encrypt::hash($request->post->senha);

            if ($user->save()) {
                // inactive token
                $getUserReset->ativo = 2;
                $getUserReset->save();

                setFlash("success", "Sua senha foi alterada com sucesso!");
                redirect("/");
            } else {
                setFlash("danger", "Ocorreu um erro ao tentar resetar sua senha. <br /> Error: {$user->fail()->getMessage()}");
            }
        }

        View::make("auth.resetpassword");
    }
}