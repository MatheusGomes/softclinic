<?php


namespace Source\Controllers;


use Source\Core\Controller;
use Source\Core\Session;
use Source\Core\View;
use Source\Models\ChatModel;
use Source\Models\ChatResponseModel;
use Source\Models\ParticipantModel;
use Source\Models\UserModel;

/**
 * Class ChatController
 * @package Source\Controllers
 */
class ChatController extends Controller
{
    /**
     * PanelController constructor.
     */
    public function __construct()
    {
        nonLogged();
        parent::__construct();
    }

    /**
     * PanelController index method
     */
    public function index()
    {
        $chats = (new ChatModel())->listChats(Session::get("auth"));

        View::make("chat.index", ["chats" => $chats]);
    }

    public function register($request)
    {
        $users = (new UserModel())->findNotNull("cdparticipante")->fetch(true);
        $listUsers = [];

        if ($users) {
            /** @var $userItem UserModel*/
            foreach ($users as $userItem) {
                if ($userItem->participant()->cdtipoparticipante !== 1 &&
                    $userItem->data()->cdusuario !== Session::get("auth")["cdusuario"]) {
                    $listUsers[] = (object) array_merge(
                        ["nmparticipante" => $userItem->participant()->nmparticipante],
                        ["cdusuario" => $userItem->data()->cdusuario]
                    );
                }
            }
        }

        if ($request->post) {
            if (!required(["destinatario", "content"], (array)$request->post)) {
                setFlash("warning", "Exite campos em brancos, preencha por favor!");
                redirect("/panel/chats/register");
                exit();
            }

            $responseChat = new ChatModel();
            $responseChat->cdusuario_remetente = Session::get("auth")["cdusuario"];
            $responseChat->cdusuario_destinatario = $request->post->destinatario;
            $responseChat->conteudo = $request->post->content;

            if ($responseChat->save()) {
                setFlash("success", "Sua menssagem foi enviada!");
                redirect("/panel/chats/view/{$responseChat->cdmensagem}");
            } else {
                setFlash("danger", "Ocorreu um erro ao tentar enviar sua mensagem. Error: {$responseChat->fail()->getMessage()}");
            }
        }

        View::make("chat.register", ["users" => $listUsers]);
    }

    public function view($chatSelected, $request)
    {
        $chat = (new ChatModel())->getChat($chatSelected->chatId, Session::get("auth"));

        if (!$chat) {
            setFlash("warning", "A conversa que você tentou acessar não existe!");
            redirect("/panel/chats");
            exit();
        }

        if ($request->post) {
            if (!required(["content"], (array)$request->post)) {
                setFlash("warning", "Exite campos em brancos, preencha por favor!");
                redirect("/panel/chats/view/{$chatSelected->chatId}");
                exit();
            }

            $responseChat = new ChatResponseModel();

            if ($this->response($chatSelected->chatId, $request, $responseChat)) {
                $json = ["message" => ["conteudo" => $responseChat->conteudo, "author" => Session::get("auth")["nome"], "dataEnvio" => $responseChat->dtenvio]];
                echo json_encode($json);
                return;
            } else {
                $json = ["error" => "Ocorreu um erro ao tentar enviar sua mensagem. Error: {$responseChat->fail()->getMessage()}"];
                echo json_encode($json);
                return;
            }
        }

        View::make("chat.view", ["chat" => $chat]);
    }

    protected function response(int $chatId, object $request, ChatResponseModel $responseChat): bool
    {
        $responseChat->cdmensagem = $chatId;
        $responseChat->cdremetente = Session::get("auth")["cdusuario"];
        $responseChat->conteudo = $request->post->content;

        if ($responseChat->save()) {
            return true;
        }

        return false;
    }


    public function viewJson($chatId, $request)
    {
        $dataset = array_merge(Session::get("auth"), ["datetime" => $request->get->datetime]);
        $chat = (new ChatModel())->getChat($chatId->chatId, $dataset);

        if (!$chat) {
            setFlash("warning", "A conversa que você tentou acessar não existe!");
            redirect("/panel/chats");
            return;
        }

        echo json_encode($chat);
        return;
    }
}