<?php


namespace Source\Controllers;


use Source\Core\Controller;
use Source\Core\View;

/**
 * Class ErrorController
 * @package Source\Controllers
 */
class ErrorController extends Controller
{

    /**
     * ErrorController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * index controller
     * @param $request
     */
    public function index($request)
    {
        View::make("errors.{$request->errorCode}");
    }
}