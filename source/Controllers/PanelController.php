<?php


namespace Source\Controllers;


use Source\Core\Controller;
use Source\Core\Session;
use Source\Core\View;

/**
 * Class PanelController
 * @package Source\Controllers
 */
class PanelController extends Controller
{
    /**
     * PanelController constructor.
     */
    public function __construct()
    {
        nonLogged();
        parent::__construct();
    }

    /**
     * PanelController index method
     */
    public function index()
    {
        hasPermission('view-panel');
        View::make("panel.index");
    }

}