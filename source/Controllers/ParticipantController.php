<?php


namespace Source\Controllers;


use Source\Core\Controller;
use Source\Core\View;
use Source\Models\CityModel;
use Source\Models\ParticipantModel;
use Source\Models\SexModel;
use Source\Models\TypeParticipant;

/**
 * Class ParticipantController
 * @package Source\Controllers
 */
class ParticipantController extends Controller
{
    /**
     * ParticipantController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * index controller
     */
    public function index()
    {
        hasPermission('list-participants');

        $participants = (new ParticipantModel())->find()->fetch(true);
        /** @var $participantsItem ParticipantModel */
        foreach ($participants as $participantsItem) {
            $listParticipants[] = (object) array_merge(
                (array) $participantsItem->data(),
                (array) $participantsItem->city()->data(),
                (array) $participantsItem->typeParticipant()->data()
            );
        }

        View::make("participant.index", ["participant" => $listParticipants]);
    }

    /**
     * register controller
     */
    public function register($request)
    {
        hasPermission('create-participants');

        $city = (new CityModel())->find()->fetch(true);
        $typeParticipant = (new TypeParticipant())->find()->fetch(true);
        $sexParticipant = (new SexModel())->find()->fetch(true);

        foreach ($city as $cityItem) {
            $listCities[] = $cityItem->data();
        }

        foreach ($typeParticipant as $typeParticipantItem) {
            $listParticipants[] = $typeParticipantItem->data();
        }

        foreach ($sexParticipant as $sexParticipantItem) {
            $listSexParticipant[] = $sexParticipantItem->data();
        }

        if ($request->post) {
            $required = ["nome", "cep", "rua", "bairro", "cpf", "telefone"];

            if (!required($required, (array)$request->post)) {
                setFlash("warning", "Exite campos em brancos, preencha por favor!");
                redirect("/panel/participants/register");
                exit();
            }

            $participantCreate = new ParticipantModel();
            $participantCreate->cdtipoparticipante = $request->post->participante;
            $participantCreate->nmparticipante = $request->post->nome;
            $participantCreate->email = $request->post->email;
            $participantCreate->cdsexo = $request->post->sexo;
            $participantCreate->numcep = $request->post->cep;
            $participantCreate->nmlogradouro = $request->post->rua;
            $participantCreate->nmbairro = $request->post->bairro;
            $participantCreate->cdcidade = $request->post->cidade;
            $participantCreate->cpfparticipante = $request->post->cpf;
            $participantCreate->numcelular = $request->post->telefone;

            if (!$participantCreate->unique("cpfparticipante")) {
                setFlash("warning", "Já exite um participante com esse <strong>CPF</strong>!");
                redirect("/panel/participants/register");
                exit();
            }

            if (!$participantCreate->unique("email")) {
                setFlash("warning", "Já exite um participante com esse <strong>Email</strong>!");
                redirect("/panel/participants/register");
                exit();
            }

            if ($participantCreate->save()) {
                setFlash("success", "O participante foi salvo com sucesso!");
            } else {
                setFlash("danger", "Ocorreu um erro ao tentar salvar. {$participantCreate->fail()->getMessage()}");
            }
        }

        View::make("participant.register", ["city" => $listCities, "typeParticipant" => $listParticipants, "sexParticipant" => $listSexParticipant]);
    }

    public function edit($participantSelected, $request)
    {
        hasPermission('edit-participants');

        $participantEdit = (new ParticipantModel())->findById($participantSelected->participantId);

        if (!$participantEdit) {
            setFlash("warning", "O participante que você tentou editar não existe!");
            redirect("/panel/participants");
            exit();
        }

        $city = (new CityModel())->find()->fetch(true);
        $typeParticipant = (new TypeParticipant())->find()->fetch(true);
        $sexParticipant = (new SexModel())->find()->fetch(true);

        foreach ($city as $cityItem) {
            $listCities[] = $cityItem->data();
        }

        foreach ($typeParticipant as $typeParticipantItem) {
            $listParticipants[] = $typeParticipantItem->data();
        }

        foreach ($sexParticipant as $sexParticipantItem) {
            $listSexParticipant[] = $sexParticipantItem->data();
        }

        if ($request->post) {
            $required = ["nome", "cep", "rua", "bairro", "cpf", "telefone"];

            if (!required($required, (array) $request->post)) {
                setFlash("warning", "Exite campos em brancos, preencha por favor!");
                redirect("/panel/participants/edit/{$participantSelected->participantId}");
                exit();
            }

            $participantEdit->cdtipoparticipante = $request->post->participante;
            $participantEdit->nmparticipante = $request->post->nome;
            $participantEdit->email = $request->post->email;
            $participantEdit->cdsexo = $request->post->sexo;
            $participantEdit->numcep = $request->post->cep;
            $participantEdit->nmlogradouro = $request->post->rua;
            $participantEdit->nmbairro = $request->post->bairro;
            $participantEdit->cdcidade = $request->post->cidade;
            $participantEdit->cpfparticipante = $request->post->cpf;
            $participantEdit->numcelular = $request->post->telefone;

            if ($participantEdit->save()) {
                setFlash("success", "O participante foi salvo com sucesso!");
            } else {
                setFlash("danger", "Ocorreu um erro ao tentar salvar. Error: {$participantEdit->fail()->getMessage()}");
            }
        }

        View::make("participant.register", ["participant" => $participantEdit, "city" => $listCities, "typeParticipant" => $listParticipants, "sexParticipant" => $listSexParticipant]);
    }

    public function delete($participantSelected)
    {
        hasPermission('delete-participants');

        $participantDelete = (new ParticipantModel())->findById($participantSelected->participantId);

        if (!$participantDelete) {
            setFlash("warning", "O participante que você tentou deletar não existe!");
            redirect("/panel/participants");
            exit();
        }

        if ($participantDelete->destroy()) {
            setFlash("success", "Participante deletado com sucesso!");
            redirect("/panel/participants");
            exit();
        } else {
            setFlash("danger", "Ocorreu um erro ao tentar deletar. Error: {$participantDelete->fail()->getMessage()}");
        }
    }
}