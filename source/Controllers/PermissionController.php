<?php


namespace Source\Controllers;


use Source\Core\Controller;
use Source\Core\View;
use Source\Models\GroupHasPermissionModel;
use Source\Models\GroupPermissionModel;
use Source\Models\PermissionModel;
use Source\Models\UserModel;

/**
 * Class PermissionController
 * @package Source\Controllers
 */
class PermissionController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * index controller
     */
    public function index()
    {
        hasPermission('list-permissions');

        $groups = (new GroupPermissionModel())->find()->fetch(true);

        View::make("permission.index", ["groups" => $groups]);
    }

    /**
     * register controller
     * @param $request
     */
    public function register($request)
    {
        hasPermission('create-permissions');

        if ($request->post) {
            $required = ["nome", "permissoes"];

            if (!required($required, (array)$request->post)) {
                setFlash("warning", "Exite campos em brancos, preencha por favor!");
                redirect("/panel/permissions/register");
                exit();
            }

            /** @var $groupPermission GroupPermissionModel */
            $groupPermission = new GroupPermissionModel();
            $groupPermission->nmgrupopermissao = $request->post->nome;

            if (!$groupPermission->unique("nmgrupopermissao")) {
                setFlash("warning", "Já exite um grupo com esse <strong>nome</strong>!");
                redirect("/panel/permissions/register");
                exit();
            }

            if ($groupPermission->save()) {
                /** @var $groupHasPermission GroupHasPermissionModel */
                $groupHasPermission = (new PermissionModel())->addItems($groupPermission, $request->post->permissoes);

                if ($groupHasPermission) {
                    setFlash("success", "Grupo cadastrado com sucesso!");
                } else {
                    setFlash("danger", "Ocorreu um erro ao tentar salvar. <br> Error: {$groupPermission->fail()->getMessage()}");
                }
            } else {
                setFlash("danger", "Ocorreu um erro ao tentar salvar. <br> Error: {$groupPermission->fail()->getMessage()}");
            }
        }

        $permissions = (new PermissionModel())->listItems();

        $listPermissions = $permissions['permissions'];

        View::make("permission.register", ["permissions" => $listPermissions]);
    }

    /**
     * @param $groupPermissionSelected
     * @param $request
     */
    public function edit($groupPermissionSelected, $request)
    {
        hasPermission('edit-permissions');

        /** @var $groupPermission GroupPermissionModel */
        $groupPermission = (new GroupPermissionModel())->findById($groupPermissionSelected->permissionId);

        if (!$groupPermission) {
            setFlash("warning", "O grupo que você tentou editar não existe!");
            redirect("/panel/permissions");
            exit();
        }

        if ($request->post) {
            if (!required(["nome", "permissoes"], (array)$request->post)) {
                setFlash("warning", "Exite campos em brancos, preencha por favor!");
                redirect("/panel/permissions/edit/{$groupPermissionSelected->permissionId}");
                exit();
            }

            $groupPermission->nmgrupopermissao = $request->post->nome;

            if ($groupPermission->save()) {
                /** @var $deletePermissions GroupHasPermissionModel */
                $deletePermissions = (new PermissionModel())->deleteItems($groupPermission);

                if ($deletePermissions) {
                    /** @var $addPermissions GroupHasPermissionModel */
                    $addPermissions = (new PermissionModel())->addItems($groupPermission, $request->post->permissoes);

                    if ($addPermissions) {
                        setFlash("success", "O usuário foi salvo com sucesso!");
                    } else {
                        setFlash("error", "Ocorreu um erro ao tentar salvar. Error: {$addPermissions->fail()->getMessage()}");
                    }
                } else {
                    setFlash("error", "Ocorreu um erro ao tentar salvar. Error: {$deletePermissions->fail()->getMessage()}");
                }

            } else {
                setFlash("error", "Ocorreu um erro ao tentar salvar. Error: {$groupPermission->fail()->getMessage()}");
            }
        }

        $permissions = (new PermissionModel())->listItems($groupPermission);
        $listPermissions = $permissions['permissions'];
        $hasPermissions = $permissions['hasPermissions'];

        View::make("permission.register", ["groupData" => $groupPermission->data(), "permissions" => $listPermissions, "hasPermissions" => $hasPermissions]);
    }

    /**
     * @param $groupPermissionSelected
     */
    public function delete($groupPermissionSelected)
    {
        hasPermission('delete-permissions');

        /** @var $groupPermission GroupPermissionModel */
        $groupPermission = (new GroupPermissionModel())->findById($groupPermissionSelected->permissionId);

        if (!$groupPermission) {
            setFlash("warning", "O grupo que você tentou deletar não existe!");
            redirect("/panel/permissions");
            exit();
        }

        /** @var $groupHasPermission GroupHasPermissionModel */
        $groupHasPermission = (new PermissionModel())->deleteItems($groupPermission);

        if ($groupHasPermission) {
            if ($groupPermission->destroy()) {
                setFlash("success", "Grupo deletado com sucesso!");
                redirect("/panel/permissions");
                exit();
            } else {
                setFlash("error", "Ocorreu um erro ao tentar deletar. Error: {$groupPermission->fail()->getMessage()}");
            }
        } else {
            setFlash("error", "Ocorreu um erro ao tentar deletar. Error: {$groupHasPermission->fail()->getMessage()}");
        }
    }
}