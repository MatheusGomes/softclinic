<?php


namespace Source\Controllers;


use Source\Core\Controller;
use Source\Core\Encrypt;
use Source\Core\Session;
use Source\Core\View;
use Source\Models\GroupPermissionModel;
use Source\Models\UserModel;

/**
 * Class UserController
 * @package Source\Controllers
 */
class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * index controller
     */
    public function index()
    {
        hasPermission('list-users');

        $users = (new UserModel())->find()->fetch(true);

        /** @var $userItem UserModel */
        foreach ($users as $userItem) {
            $listUsers[] = (object) array_merge(
                (array) $userItem->data(),
                (array) $userItem->groupPermission()->data()
            );
        }

        View::make("user.index", ["users" => $listUsers]);
    }

    /**
     * register controller
     * @param $request
     */
    public function register($request)
    {
        hasPermission('create-users');

        $groups = (new GroupPermissionModel())->find()->fetch(true);

        foreach ($groups as $groupItem) {
            $listGroups[] = $groupItem->data();
        }

        if ($request->post) {
            if (!isset($request->post->grupo) && empty($request->post->grupo)) {
                setFlash("warning", "Selecione o <strong>grupo</strong> do usuário!");
                redirect("/panel/users/register");
                exit();
            }

            if ($request->post->senha !== $request->post->confirmaSenha) {
                setFlash("warning", "As senhas não conferem!");
                redirect("/panel/users/register");
                exit();
            }

            $user = new UserModel();
            $user->cdgrupopermissao = $request->post->grupo;
            $user->emailusuario = $request->post->email;
            $user->senhausuario = Encrypt::hash($request->post->senha);

            if (!$user->unique("emailusuario")) {
                setFlash("warning", "Já exite um usuário com esse <strong>Email</strong>!");
                redirect("/panel/users/register");
                exit();
            }

            if ($user->save()) {
                setFlash("success", "Usuário cadastrado com sucesso!");
            } else {
                setFlash("danger", "Ocorreu um erro ao tentar salvar. <br> Error: {$user->fail()->getMessage()}");
            }
        }

        View::make("user.register", ["groups" => $listGroups]);
    }

    /**
     * @param $userSelected
     * @param $request
     */
    public function edit($userSelected, $request)
    {
        if (Session::get("auth")["cdusuario"] != $userSelected->userId) {
            hasPermission('edit-users');
        }

        $user = (new UserModel())->findById($userSelected->userId);

        if (!$user) {
            setFlash("warning", "O usuário que você tentou editar não existe!");
            redirect("/panel/users");
            exit();
        }

        $listUser = $user->data();
        $groups = (new GroupPermissionModel())->find()->fetch(true);

        foreach ($groups as $groupItem) {
            $listGroups[] = $groupItem->data();
        }

        if ($request->post) {
            $required = ["email"];
            if (hasPermission("list-permissions", false)) {
                $required = array_merge($required, ["grupo"]);
            }

            if (!required($required, (array)$request->post)) {
                setFlash("warning", "Campos e-mail e grupo são obrigatórios!");
                redirect("/panel/users/edit/{$userSelected->userId}");
                exit();
            }

            if ($request->post->senha !== $request->post->confirmaSenha) {
                setFlash("warning", "As senhas não conferem!");
                redirect("/panel/users/edit/{$userSelected->userId}");
                exit();
            }

            $user->cdgrupopermissao = ($request->post->grupo) ?? $user->cdgrupopermissao;
            $user->emailusuario = $request->post->email;
            $user->senhausuario = ($request->post->senha) ? Encrypt::hash($request->post->senha) : $user->senhausuario;

            if ($user->save()) {
                setFlash("success", "O usuário foi salvo com sucesso!");
            } else {
                setFlash("error", "Ocorreu um erro ao tentar salvar. Error: {$user->fail()->getMessage()}");
            }
        }

        View::make("user.register", ["user" => $listUser, "groups" => $listGroups]);
    }

    /**
     * @param $userSelected
     */
    public function delete($userSelected)
    {
        hasPermission('delete-users');

        $user = (new UserModel())->findById($userSelected->userId);

        if (!$user) {
            setFlash("warning", "O usuário que você tentou deletar não existe!");
            redirect("/panel/users");
            exit();
        }

        if ($user->destroy()) {
            setFlash("success", "Usuário deletado com sucesso!");
            redirect("/panel/users");
            exit();
        } else {
            setFlash("error", "Ocorreu um erro ao tentar deletar. Error: {$user->fail()->getMessage()}");
        }
    }
}