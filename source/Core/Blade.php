<?php

namespace Source\Core;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\Container as ContainerInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory as FactoryContract;
use Illuminate\Contracts\View\View;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Facade;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Factory;
use Illuminate\View\ViewServiceProvider;

/**
 * Class Blade
 * @package Source\Core
 */
class Blade implements FactoryContract
{
    /**
     * @var Application
     */
    protected $container;

    /**
     * @var Factory
     */
    private $factory;

    /**
     * @var BladeCompiler
     */
    private $compiler;

    /**
     * Blade constructor.
     * @param $viewPaths
     * @param string $cachePath
     * @param ContainerInterface|null $container
     */
    public function __construct($viewPaths, string $cachePath, ContainerInterface $container = null)
    {
        $this->container = $container ?: new Container;

        $this->setupContainer((array) $viewPaths, $cachePath);
        (new ViewServiceProvider($this->container))->register();

        $this->factory = $this->container->get('view');
        $this->compiler = $this->container->get('blade.compiler');
    }

    /**
     * @param string $view
     * @param array $data
     * @param array $mergeData
     * @return string
     */
    public function render(string $view, array $data = [], array $mergeData = []): string
    {
        return $this->make($view, $data, $mergeData)->render();
    }

    /**
     * @param string $view
     * @param array $data
     * @param array $mergeData
     * @return View
     */
    public function make($view, $data = [], $mergeData = []): View
    {
        return $this->factory->make($view, $data, $mergeData);
    }

    /**
     * @return BladeCompiler
     */
    public function compiler(): BladeCompiler
    {
        return $this->compiler;
    }

    /**
     * @param string $name
     * @param callable $handler
     */
    public function directive(string $name, callable $handler)
    {
        $this->compiler->directive($name, $handler);
    }

    /**
     * @param $name
     * @param callable $callback
     */
    public function if($name, callable $callback)
    {
        $this->compiler->if($name, $callback);
    }

    /**
     * @param string $view
     * @return bool
     */
    public function exists($view): bool
    {
        return $this->factory->exists($view);
    }

    /**
     * @param string $path
     * @param array $data
     * @param array $mergeData
     * @return View
     */
    public function file($path, $data = [], $mergeData = []): View
    {
        return $this->factory->file($path, $data, $mergeData);
    }

    /**
     * @param array|string $key
     * @param null $value
     * @return mixed
     */
    public function share($key, $value = null)
    {
        return $this->factory->share($key, $value);
    }

    /**
     * @param array|string $views
     * @param \Closure|string $callback
     * @return array
     */
    public function composer($views, $callback): array
    {
        return $this->factory->composer($views, $callback);
    }

    /**
     * @param array|string $views
     * @param \Closure|string $callback
     * @return array
     */
    public function creator($views, $callback): array
    {
        return $this->factory->creator($views, $callback);
    }

    /**
     * @param string $namespace
     * @param array|string $hints
     * @return $this
     */
    public function addNamespace($namespace, $hints): self
    {
        $this->factory->addNamespace($namespace, $hints);

        return $this;
    }

    /**
     * @param string $namespace
     * @param array|string $hints
     * @return $this
     */
    public function replaceNamespace($namespace, $hints): self
    {
        $this->factory->replaceNamespace($namespace, $hints);

        return $this;
    }

    /**
     * @param string $method
     * @param array $params
     * @return mixed
     */
    public function __call(string $method, array $params)
    {
        return call_user_func_array([$this->factory, $method], $params);
    }

    /**
     * @param array $viewPaths
     * @param string $cachePath
     */
    protected function setupContainer(array $viewPaths, string $cachePath)
    {
        $this->container->bindIf('files', function () {
            return new Filesystem;
        }, true);

        $this->container->bindIf('events', function () {
            return new Dispatcher;
        }, true);

        $this->container->bindIf('config', function () use ($viewPaths, $cachePath) {
            return [
                'view.paths' => $viewPaths,
                'view.compiled' => $cachePath,
            ];
        }, true);

        Facade::setFacadeApplication($this->container);
    }
}