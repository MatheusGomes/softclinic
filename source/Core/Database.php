<?php

namespace Source\Core;

use PDO;
use PDOException;

/**
 * Class Database
 * @package Source\Core
 */
class Database {
    /**
     * @var PDO
     */
    private static $instance;
    /**
     * @var PDOException
     */
    private static $error;

    /**
     * @return PDO|null
     */
    public static function getInstance(): ?PDO
    {
        try {
            if (self::$instance == null) {
                self::$instance = new PDO(DATABASE["driver"] . ":host=" . DATABASE["host"] . ";port=" . DATABASE["port"] . ";dbname=" . DATABASE["dbname"],
                    DATABASE["username"],
                    DATABASE["passwd"],
                    DATABASE["options"]);
            }

        } catch (PDOException $exception) {
            self::$error = $exception;
        }

        return self::$instance;
    }

    /**
     * @return PDOException|null
     */
    public static function getError(): ?PDOException
    {
        return self::$error;
    }

    /**
     * Database constructor.
     */
    final private function __construct()
    {
    }

    /**
     * Database clone.
     */
    final private function __clone()
    {
    }
}