<?php


namespace Source\Core;


class Encrypt
{
    public static function encrypt(string $value, string $key = ""): string
    {
        $start = substr($value, 0, 2);
        $middle = substr($value, 2, -2);
        $end = substr($value, -2);

        return base64_encode($middle . $key . $start . ENCRYPT["keySecurity"] . $end);
    }

    public static function decrypt(string $value, string $key = ''): string
    {
        $decode = base64_decode($value);
        $unKey = str_replace(ENCRYPT["keySecurity"], '', str_replace($key, '', $decode));

        $start = substr($unKey, -4, 2);
        $middle = substr($unKey, 0, -4);
        $end = substr($unKey, -2);

        return $start . $middle . $end;
    }

    public static function hash(string $value): string
    {
        $onEncrypt = self::encrypt($value);
        $md5 = md5($onEncrypt);
        $crypt = crypt($value, $md5);

        return hash('sha512', $crypt);
    }
}