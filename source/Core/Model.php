<?php

namespace Source\Core;

use Exception;
use PDO;
use PDOException;
use stdClass;

/**
 * Class Model
 * @package Source\Core
 */
class Model {
    /**
     * @var string $entity database table
     */
    private string $entity;

    /**
     * @var string $primary database primary key
     */
    private string $primary;

    /**
     * @var array
     */
    private array $required;

    /**
     * @var string $statement
     */
    private string $statement;

    /**
     * @var mixed $params
     */
    protected $params;

    /**
     * @var mixed $group
     */
    protected $group;

    /**
     * @var mixed $order
     */
    protected $order;

    /**
     * @var mixed $limit
     */
    protected $limit;

    /**
     * @var mixed $offset
     */
    protected $offset;

    /**
     * @var PDOException|null
     */
    protected $fail;

    /**
     * @var object $data
     */
    protected object $data;

    /**
     * Model constructor.
     * @param string $entity
     * @param string $primary
     */
    public function __construct(string $entity, string $primary, array $required)
    {
        $this->entity = $entity;
        $this->primary = $primary;
        $this->required = $required;
    }

    /**
     * @param string|null $terms
     * @param string|null $params
     * @param string $columns
     * @return Model
     */
    public function find(?string $terms = null, ?string $params = null, string $columns = "*"): Model
    {
        if ($terms) {
            $this->statement = "SELECT {$columns} FROM {$this->entity} WHERE {$terms}";
            $params = str_replace(' ', '', $params);
            parse_str($params, $this->params);
            return $this;
        }

        $this->statement = "SELECT {$columns} FROM {$this->entity}";
        return $this;
    }

    /**
     * @param string|null $column
     * @param string $columns
     * @return Model
     */
    public function findNotNull(?string $column = null, string $columns = "*"): Model
    {
        $this->statement = "SELECT {$columns} FROM {$this->entity} WHERE {$column} IS NOT NULL";
        return $this;
    }

    /**
     * @param int $id
     * @param string|null $columns
     * @return Model|null
     */
    public function findById(int $id, ?string $columns = "*"): ?Model
    {
        $find = $this->find("{$this->primary} = :id", "id = {$id}", $columns);
        return $find->fetch();
    }

    /**
     * @param string $column
     * @return Model|null
     */
    public function group(string $column): ?Model
    {
        $this->group = " GROUP BY {$column}";

        return $this;
    }

    /**
     * @param string $columnOrder
     * @return Model|null
     */
    public function order(string $columnOrder): ?Model
    {
        $this->order = " ORDER BY {$columnOrder}";
        return $this;
    }

    /**
     * @param int $limit
     * @return Model|null
     */
    public function limit(int $limit): ?Model
    {
        $this->limit = " LIMIT {$limit}";
        return $this;
    }

    /**
     * @param int $offset
     * @return Model|null
     */
    public function offset(int $offset): ?Model
    {
        $this->offset = " LIMIT {$offset}";
        return $this;
    }


    /**
     * @param bool $all
     * @return array|mixed|null
     */
    public function fetch(bool $all = false)
    {
        try {
            $stmt = Database::getInstance()->prepare($this->statement . $this->group . $this->order . $this->limit . $this->offset);
            $stmt->execute($this->params);

            if (!$stmt->rowCount()) {
                return null;
            }

            if ($all) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, static::class);
            }

            return $stmt->fetchObject(static::class);
        } catch (PDOException $exception) {
            $this->fail = $exception;
            return null;
        }
    }

    /**
     * @return int
     */
    public function count(): int
    {
        $stmt = Database::getInstance()->prepare($this->statement);
        $stmt->execute($this->params);

        return $stmt->rowCount();
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value): void
    {
        if (empty($this->data)) {
            $this->data = new stdClass();
        }

        $this->data->$name = $value;
    }


    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return ($this->data->$name ?? null);
    }

    /**
     * @return object
     */
    public function data()
    {
        return $this->data;
    }

    /**
     * @return PDOException|null
     */
    public function fail()
    {
        return $this->fail;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function unique(string $field): bool
    {
        $unique = $this->find("{$field} = :unique{$field}", "unique{$field}={$this->$field}")->fetch();

        if ($unique) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        $primary = $this->primary;
        $id = null;

        try {
            if (!required($this->required, (array)$this->data())) {
                throw new Exception("Preencha os campos necessários");
            }

            /** Update */
            if (!empty($this->data->$primary)) {
                $id = $this->data->$primary;
                $this->update($this->safe(), $this->primary . " = :id", "id={$id}");
            }

            /** Create */
            if (empty($this->data->$primary)) {
                $id = $this->create($this->safe());
            }

            if (!$id) {
                return false;
            }

            $this->data = $this->findById($id)->data();
            return true;
        } catch (Exception $exception) {
            $this->fail = $exception;
            return false;
        }
    }

    /**
     * @return bool
     */
    public function destroy(): bool
    {
        $primary = $this->primary;
        $id = $this->data->$primary;

        if (empty($id)) {
            return false;
        }

        return $this->delete($this->primary . " = :id", "id={$id}");
    }

    /**
     * @param array $data
     * @return int|null
     * @throws Exception
     */
    protected function create(array $data): ?int
    {
        try {
            $columns = implode(", ", array_keys($data));
            $values = ":" . implode(", :", array_keys($data));

            $stmt = Database::getInstance()->prepare("INSERT INTO {$this->entity} ({$columns}) VALUES ({$values})");
            $stmt->execute($this->filter($data));

            return Database::getInstance()->lastInsertId();
        } catch (PDOException $exception) {
            $this->fail = $exception;
            return null;
        }
    }

    /**
     * @param array $data
     * @param string $terms
     * @param string $params
     * @return int|null
     * @throws Exception
     */
    protected function update(array $data, string $terms, string $params): ?int
    {
        try {
            $dateSet = [];
            foreach ($data as $bind => $value) {
                $dateSet[] = "{$bind} = :{$bind}";
            }

            $dateSet = implode(", ", $dateSet);
            parse_str($params, $params);

            $stmt = Database::getInstance()->prepare("UPDATE {$this->entity} SET {$dateSet} WHERE {$terms}");
            $stmt->execute($this->filter(array_merge($data, $params)));
            return ($stmt->rowCount() ?? 1);
        } catch (PDOException $exception) {
            $this->fail = $exception;
            return null;
        }
    }

    /**
     * @param string $terms
     * @param string|null $params
     * @return bool
     */
    protected function delete(string $terms, ?string $params): bool
    {
        try {
            $stmt = Database::getInstance()->prepare("DELETE FROM {$this->entity} WHERE {$terms}");
            if ($params) {
                parse_str($params, $params);
                $stmt->execute($params);
                return true;
            }

            $stmt->execute();
            return true;
        } catch (PDOException $exception) {
            $this->fail = $exception;
            return false;
        }
    }

    /**
     * @return array|null
     */
    protected function safe(): ?array
    {
        $safe = (array)$this->data;
        unset($safe[$this->primary]);

        return $safe;
    }

    /**
     * @param array $data
     * @return array|null
     */
    private function filter(array $data): ?array
    {
        $filter = [];
        foreach ($data as $key => $value) {
            $filter[$key] = (is_null($value) ? null : filter_var($value, FILTER_DEFAULT));
        }

        return $filter;
    }

}