<?php

namespace Source\Core;

/**
 * Class Router
 * @package Source\Core
 */
class Router
{
    /**
     * @var
     */
    protected $route;

    /**
     * @var
     */
    protected $routes;

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @var false|string
     */
    protected $projectUrl;

    /**
     * @var mixed|string
     */
    protected $patch;

    /** @var int */
    protected $error;

    /** @const int Bad Request */
    public const BAD_REQUEST = 400;

    /** @const int Not Found */
    public const NOT_FOUND = 404;

    /** @const int Method Not Allowed */
    public const METHOD_NOT_ALLOWED = 405;

    /**
     * Router constructor.
     * @param string $projectUrl
     */
    public function __construct(string $projectUrl)
    {
        $this->projectUrl = (substr($projectUrl, "-1") == "/" ? substr($projectUrl, 0, -1) : $projectUrl);
        $this->patch = (filter_input(INPUT_GET, "route", FILTER_DEFAULT) ?? "/");
    }

    /**
     * @param string $route
     * @param string $handler
     */
    public function route(string $route, string $handler): void
    {
        $this->setRoute($route, $handler);
    }

    /**
     * @return bool
     */
    public function execute(): bool
    {
        if ($this->route) {
            $this->route = explode(":", $this->route);

            $controller = "Source\\Controllers\\" . $this->route[0];
            $method = $this->route[1];

            if (class_exists($controller)) {
                $newController = new $controller();
                if (method_exists($controller, $method)) {
                    if (count($this->params) > 0) {
                        $newController->$method((object)$this->params, $this->getRequest());
                    } else {
                        $newController->$method($this->getRequest());
                    }
                    return true;
                }

                $this->error = self::METHOD_NOT_ALLOWED;
                return false;
            }


            $this->error = self::BAD_REQUEST;
            return false;
        }

        $this->error = self::NOT_FOUND;
        return false;
    }

    /**
     * @return int|null
     */
    public function error(): ?int
    {
        return $this->error;
    }

    /**
     * @param string $route
     */
    public function redirect(string $route): void
    {
        header("Location: {$this->projectUrl}{$route}");
        exit;
    }

    /**
     * @return object
     */
    private function getRequest(): object
    {
        $request = new \stdClass();
        $request->get = (!empty($_GET) ? new \stdClass() : NULL);
        $request->post = (!empty($_POST) ? new \stdClass() : NULL);

        foreach ($_GET as $key => $value) {
            $request->get->$key = (!empty($value) ? $value : NULL);
        }

        foreach ($_POST as $key => $value) {
            $request->post->$key = (!empty($value) ? $value : NULL);
        }

        return $request;
    }

    /**
     * @param string $route
     * @param string $handler
     */
    private function setRoute(string $route, string $handler): void
    {
        $this->routes[$route] = $handler;

        $patchArray = explode("/", $this->patch);

        foreach ($this->routes as $route => $handle) {
            $routeArray = explode("/", $route);
            for ($i = 0; $i < count($routeArray); $i++) {
                if((count($routeArray) == count($patchArray)) && (strpos($routeArray[$i], "{") !== false)) {
                    $this->params = [str_replace(["{", "}"], "", $routeArray[$i]) => $patchArray[$i]];
                    $routeArray[$i] = $patchArray[$i];
                }
                $this->route = implode("/", $routeArray);
            }

            if ($this->route == strtolower($this->patch)) {
                $this->route = $handle;
                break;
            }

            $this->route = false;
        }
    }
}