<?php


namespace Source\Core;


class Session
{

    /**
     * @param string $name
     * @param $value
     */
    public static function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public static function get($name)
    {
        return $_SESSION[$name];
    }

    /**
     * @return array
     */
    public static function all()
    {
        return $_SESSION;
    }

    /**
     * @param string $name
     * @return bool
     */
    public static function has($name)
    {
        return ( isset($_SESSION[$name]) ) ? true : false;
    }

    /**
     * @param string $name
     */
    public static function delete($name = '')
    {
        if ($name != ''):
            unset($_SESSION[$name]);
        else:
            session_destroy();
        endif;
    }

    /**
     *
     */
    public static function regenerate()
    {
        session_regenerate_id();
    }
}