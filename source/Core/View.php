<?php


namespace Source\Core;

/**
 * Class View
 * @package Source\Core
 */
class View
{
    /**
     * @var string
     */
    private static $views = __DIR__ . "/../../source/Views";
    /**
     * @var string
     */
    private static $cache = __DIR__ . "/../../storage/cache/";

    /**
     * @param $viewShow
     * @param array $params
     * @param bool $return
     * @return String|null
     */
    public static function make($viewShow, array $params = [], $return = false): ?String
    {
        $bladeTemplate = new Blade(self::$views, self::$cache);
        if ($return != true) {
            echo $bladeTemplate->make($viewShow, $params)->render();
        } else {
            return $bladeTemplate->make($viewShow, $params)->render();
        }

        return null;
    }
}