<?php

/**
 * @param string|null $param
 * @return string
 */
function site(string $param = null): string
{
    if ($param && !empty(SITE[$param])) {
        return SITE[$param];
    }

    return SITE["root"];
}

/**
 * @param string $path
 * @return string
 */
function asset(string $path): string
{
    return SITE["root"] . "/public/assets/{$path}";
}

/**
 * @param string|null $type
 * @param string|null $message
 * @return bool|null
 */
function setFlash(string $type = null, string $message = null): ?bool
{
    if ($type && $message) {
        $_SESSION["flash"] = [
            "type" => $type,
            "message" => $message
        ];

        return true;
    }

    return false;
}

/**
 * @return string|null
 */
function getFlash(): ?string
{
    if (!empty($_SESSION["flash"]) && $flash = $_SESSION["flash"]) {
        unset($_SESSION["flash"]);
        return "<div class=\"alert alert-{$flash["type"]}\" role=\"alert\">{$flash["message"]}</div>";
    }

    return null;
}

/**
 * @param string $route
 */
function redirect(string $route): void
{
    header("Location: " . site() . $route);
    exit;
}

/**
 * @param string $date
 * @return string
 */
function timestampsToBR(string $date): string
{
    $explodeDate = explode(' ', $date);
    $explodeTime = explode(".", $explodeDate[1]);

    $newDate = implode('/', array_reverse(explode('-', $explodeDate[0])));
    $newDate = $newDate . ' as ' . $explodeTime[0];
    $newDate = substr($newDate, 0, -3);

    return $newDate;
}

/**
 * @param string $date
 * @return string
 */
function timestampsToUS(string $date): string
{
    $dateReplace = str_replace('as', '', $date);
    $dateExplode = explode(' ', $dateReplace);
    $newDate = explode('/', $dateExplode[0]);

    if (empty($dateExplode[1])) :
        $dateFormat[1] = date('H:i:s');
    endif;

    $newDate = $newDate[2] . '-' . $newDate[1] . '-' . $newDate[0] . ' ' . $dateFormat[1];

    return $newDate;
}

/**
 * Non Logged user
 */
function nonLogged(): void
{
    if (!\Source\Core\Session::has("auth")) {
        setFlash("danger", "Você precisa está logado para acessar essa página!");
        redirect("/");
    }
}

/**
 * @param string $requiredPermission
 * @param bool $redirect
 * @return bool
 */
function hasPermission(string $requiredPermission, bool $redirect = true): bool
{
    $listPermissions = \Source\Core\Session::get("auth")["permissions"];

    if (!in_array($requiredPermission, $listPermissions)) {
        if ($redirect === true) redirect("/oops/403");
        return false;
    }

    return true;
}

/**
 * @param array $inputs
 * @param array $request
 * @return bool
 */
function required(array $inputs, array $request): bool
{
    foreach ($inputs as $field) {
        if (empty($request[$field])) {
            return false;
        }
    }

    return true;
}