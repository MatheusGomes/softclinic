<?php


namespace Source\Models;


use Source\Core\Model;

/**
 * Class AnamneseModel
 * @package Source\Models
 */
class AnamneseModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "anamneses";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdanamnese";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * ParticipantModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }

    /**
     * @return ParticipantModel|null
     */
    public function participant(): ?ParticipantModel
    {
        return (new ParticipantModel())->find("cdparticipante = :participantId", "participantId={$this->cdparticipante}")->fetch();
    }

    /**
     * @return AnimalModel|null
     */
    public function animal(): ?AnimalModel
    {
        return (new AnimalModel())->find("cdanimal = :animalId", "animalId={$this->cdanimal}")->fetch();
    }
}