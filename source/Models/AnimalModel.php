<?php


namespace Source\Models;


use Source\Core\Model;

/**
 * Class AnimalModel
 * @package Source\Models
 */
class AnimalModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "animais";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdanimal";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * CityModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }
}