<?php


namespace Source\Models;


use Source\Core\Model;

/**
 * Class ChatModel
 * @package Source\Models
 */
class ChatModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "mensagens";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdmensagem";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * ParticipantModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }

    /**
     * @param array $session
     * @return array|null
     */
    public function listChats(array $session): ?array
    {
        $chats = $this->find(
            "cdusuario_destinatario = :userId OR cdusuario_remetente = :userId",
            "userId={$session["cdusuario"]}")
            ->fetch(true);

        $listChats = [];

        if ($chats) {
            /** @var $chatsItem ChatModel */
            foreach ($chats as $chatsItem) {
                $listChats[] = (object)array_merge(
                    (array)$chatsItem->data(),
                    [
                        "nomedestinatario" => ($chatsItem->recipient()->participant()) ?
                            $chatsItem->recipient()->participant()->data()->nmparticipante : "Sem participante associado"
                    ],
                    [
                        "nomeremetente" => ($chatsItem->sender()->participant()) ?
                            $chatsItem->sender()->participant()->data()->nmparticipante : "Sem participante associado"
                    ]
                );
            }
        }

        return $listChats;
    }

    /**
     * @param int $chatId
     * @return array|null
     */
    public function getChat(int $chatId, array $session): ?object
    {
        /** @var $chat ChatModel */
        $chat = $this->find(
            "cdmensagem = :messageId",
            "&messageId={$chatId}")
            ->fetch();

        if (!$chat) {
            return null;
        }

        if (isset($session["datetime"])) {
            $chatResponses = (new ChatResponseModel())
                ->find(
                    "cdmensagem = :messageId AND dtenvio >= :datetime",
                    "messageId={$chat->cdmensagem}&datetime={$session["datetime"]}",
                    "cdremetente, conteudo, dtenvio"
                )
                ->order("dtenvio ASC")
                ->fetch(true);
        } else {
            $chatResponses = (new ChatResponseModel())
                ->find(
                    "cdmensagem = :messageId",
                    "messageId={$chat->cdmensagem}",
                    "cdremetente, conteudo, dtenvio"
                )
                ->order("dtenvio ASC")
                ->fetch(true);
        }


        $listResponses["responses"] = [];
        if ($chatResponses) {
            /** @var $chatResponsesItem ChatResponseModel */
            foreach ($chatResponses as $chatResponsesItem) {
                $listResponses["responses"][] = (object)array_merge(
                    (array)$chatResponsesItem->data()
                );
            }
        }

        $sender = [
            "nomeremetente" => ($chat->sender()->participant()) ?
                $chat->sender()->participant()->data()->nmparticipante : "Sem participante associado"
        ];
        $recipient = [
            "nomedestinatario" => ($chat->recipient()->participant()) ?
                $chat->recipient()->participant()->data()->nmparticipante : "Sem participante associado"
        ];

        return (object)array_merge(
            (array)$chat->data(),
            $sender,
            $recipient,
            $listResponses,
            ["user_logged" => $session["cdusuario"]]
        );
    }

    /**
     * @return UserModel|null
     */
    public function sender(): ?UserModel
    {
        return (new UserModel())
            ->find("cdusuario = :senderId", "senderId={$this->cdusuario_remetente}", "cdparticipante")
            ->fetch();
    }

    /**
     * @return UserModel|null
     */
    public function recipient(): ?UserModel
    {
        return (new UserModel())
            ->find("cdusuario = :recipientId", "recipientId={$this->cdusuario_destinatario}", "cdparticipante")
            ->fetch();
    }
}