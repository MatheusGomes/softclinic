<?php


namespace Source\Models;


use Source\Core\Model;

/**
 * Class ChatResponseModel
 * @package Source\Models
 */
class ChatResponseModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "mensagensrespostas";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdmensagemresposta";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * ParticipantModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }

    /**
     * @return UserModel|null
     */
    public function sender(): ?UserModel
    {
        return (new UserModel())->find("cdusuario = :senderId", "senderId={$this->cdremetente}", "emailusuario AS emailsender")->fetch();
    }
}