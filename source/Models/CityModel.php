<?php


namespace Source\Models;


use Source\Core\Model;

/**
 * Class CityModel
 * @package Source\Models
 */
class CityModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "cidades";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdCidade";

    /**
     * @var array $required required inputs table
     */
    private array $required = ['nmCidade'];

    /**
     * CityModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }
}