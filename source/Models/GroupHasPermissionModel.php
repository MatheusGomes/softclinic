<?php


namespace Source\Models;


use Source\Core\Model;

/**
 * Class GroupHasPermissionModel
 * @package Source\Models
 */
class GroupHasPermissionModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "grupotempermissoes";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdgrupotempermissao";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * CityModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }

    /**
     * @param GroupPermissionModel $groupPermission
     * @param int $permission
     * @return GroupHasPermissionModel
     */
    public function add(GroupPermissionModel $groupPermission, int $permission): GroupHasPermissionModel
    {
        $this->cdgrupopermissao = $groupPermission->cdgrupopermissao;
        $this->cdpermissao = $permission;

        return $this;
    }

}