<?php


namespace Source\Models;


use Source\Core\Model;

/**
 * Class GroupPermissionModel
 * @package Source\Models
 */
class GroupPermissionModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "grupospermissoes";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdgrupopermissao";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * CityModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }
}