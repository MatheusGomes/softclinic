<?php


namespace Source\Models;


use Source\Core\Model;

/**
 * Class ParticipantModel
 * @package Source\Models
 */
class ParticipantModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "participantes";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdparticipante";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * ParticipantModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);

    }

    /**
     * @return CityModel|null
     */
    public function city(): ?CityModel
    {
        return (new CityModel())->find("cdcidade = :cidadeId", "cidadeId={$this->cdcidade}")->fetch();
    }

    /**
     * @return TypeParticipant|null
     */
    public function typeParticipant(): ?TypeParticipant
    {
        return (new TypeParticipant())->find("cdtipoparticipante = :tipoParticipantId", "tipoParticipantId={$this->cdtipoparticipante}")->fetch();
    }

    /**
     * @param CityModel $city
     * @return ParticipantModel
     */
    public function add(CityModel $city): ParticipantModel
    {
        $this->cdcidade = $city->cdcidade;

        return $this;
    }
}