<?php


namespace Source\Models;


use Source\Core\Model;

/**
 * Class PermissionModel
 * @package Source\Models
 */
class PermissionModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "permissoes";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdpermissao";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * CityModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }


    /**
     * @param GroupPermissionModel|null $groupPermission
     * @return array|null
     */
    public function listItems(?GroupPermissionModel $groupPermission = null): ?array
    {
        $permissions = $this->find()->fetch(true);
        $groupHasPermissions = (!$groupPermission) ? null : (new GroupHasPermissionModel())->find("cdgrupopermissao = :groupId", "groupId={$groupPermission->cdgrupopermissao}")->fetch(true);

        $object = [
            "permissions" => [],
            "hasPermissions" => []
        ];

        foreach ($permissions as $permissionItem) {
            $object["permissions"][] = $permissionItem->data();

            if ($groupHasPermissions) {
                foreach ($groupHasPermissions as $hasPermissionsItem) {
                    $object['hasPermissions'][] = $hasPermissionsItem->data()->cdpermissao;
                }
            }
        }

        return $object;
    }

    /**
     * @param GroupPermissionModel $groupPermission
     * @param array $permissions
     * @return bool
     */
    public function addItems(GroupPermissionModel $groupPermission, array $permissions): bool
    {
        try {
            foreach ($permissions as $permission) {
                $object = new GroupHasPermissionModel();
                $object->add($groupPermission, $permission);
                $object->save();
            }

            return true;
        } catch (\Exception $exception) {
            $this->fail = $exception;
        }

        return false;
    }

    /**
     * @param GroupPermissionModel $groupPermission
     * @return bool
     */
    public function deleteItems(GroupPermissionModel $groupPermission): bool
    {
        try {
            $object = (new GroupHasPermissionModel())->find("cdgrupopermissao = :groupId", "groupId={$groupPermission->cdgrupopermissao}")->fetch(true);

            foreach ($object as $objectItem) {
                $objectItem->destroy();
            }

            return true;
        } catch (\Exception $exception) {
            $this->fail = $exception;
        }

        return false;
    }
}