<?php


namespace Source\Models;


use Source\Core\Model;

class ResetPasswordModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "resetarsenha";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdresetarsenha";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * UserModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }

    public function updateState(int $id, int $state): bool
    {
        $updateToken = (new ResetPasswordModel())->findById($id);
        $updateToken->ativo = $state;

        return $updateToken->save();
    }

}