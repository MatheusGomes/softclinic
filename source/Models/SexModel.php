<?php


namespace Source\Models;


use Source\Core\Model;

class SexModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "sexos";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdsexo";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * CityModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }
}