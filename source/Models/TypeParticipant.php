<?php


namespace Source\Models;


use Source\Core\Model;

class TypeParticipant extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "tiposparticipantes";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdtipoparticipante";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * CityModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);
    }
}