<?php

namespace Source\Models;


use Source\Core\Model;

/**
 * Class UserModel
 * @package Source\Models
 */
class UserModel extends Model
{
    /**
     * @var string $entity table name
     */
    private string $entity = "usuarios";

    /**
     * @var string $primary primary key table
     */
    private string $primaryKey = "cdusuario";

    /**
     * @var array $required required inputs table
     */
    private array $required = [];

    /**
     * UserModel constructor.
     */
    public function __construct()
    {
        parent::__construct($this->entity, $this->primaryKey, $this->required);

    }

    /**
     * @param string $mail
     * @param string $password
     * @return UserModel|null
     */
    public function auth(string $mail, string $password): ?UserModel
    {
        $user = $this->find("emailusuario = :mail", "mail={$mail}")->fetch();

        if ($user && $user->senhausuario == $password) {
            return $user;
        }

        return null;
    }

    /**
     * @return GroupPermissionModel|null
     */
    public function groupPermission(): ?GroupPermissionModel
    {
        return (new GroupPermissionModel())->find("cdgrupopermissao = :groupId", "groupId={$this->cdgrupopermissao}")->fetch();
    }

    /**
     * @return array|null
     */
    public function listPermissions(): ?array
    {
        $group = (new GroupHasPermissionModel())->find("cdgrupopermissao = :groupId", "groupId={$this->cdgrupopermissao}", "cdpermissao")->fetch(true);
        $lstPermissions = [];

        /** @var $groupItem GroupHasPermissionModel */
        foreach ($group as $groupItem) {
            $lstPermissions[] = (new PermissionModel())->find("cdpermissao = :permissionId", "permissionId={$groupItem->data()->cdpermissao}")->fetch()->data()->slugpermissao;
        }

        return $lstPermissions;
    }

    public function userHasParticipant(): ?object
    {
        return $this->find("emailusuario = :userEmail", "userEmail={$this->emailusuario}")->fetch();
    }

    /**
     * @return ParticipantModel|null
     */
    public function participant(): ?ParticipantModel
    {
        return (new ParticipantModel())->find("cdparticipante = :participantId", "participantId={$this->cdparticipante}")->fetch();
    }

    /**
     * @param ParticipantModel $participant
     * @return UserModel
     */
    public function add(ParticipantModel $participant): UserModel
    {
        $this->cdparticipante = $participant->cdparticipante;

        return $this;
    }

}