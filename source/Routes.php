<?php

use Source\Core\Router;

$Route = new Router(site());

// group /
$Route->route("/", "AuthController:login");
$Route->route("/register", "AuthController:register");
$Route->route("/complete-register", "AuthController:completeRegister");
$Route->route("/recovery", "AuthController:recovery");
$Route->route("/resetpassword/{token}", "AuthController:resetPassword");
$Route->route("/logout", "AuthController:logout");
$Route->route("/panel", "PanelController:index");

// group Participant
$Route->route("/panel/participants", "ParticipantController:index");
$Route->route("/panel/participants/register", "ParticipantController:register");
$Route->route("/panel/participants/edit/{participantId}", "ParticipantController:edit");
$Route->route("/panel/participants/delete/{participantId}", "ParticipantController:delete");

//group chats
$Route->route("/panel/chats", "ChatController:index");
$Route->route("/panel/chats/register", "ChatController:register");
$Route->route("/panel/chats/view/{chatId}", "ChatController:view");
$Route->route("/panel/chats/delete/{chatId}", "ChatController:delete");
$Route->route("/panel/chats/messages/{chatId}", "ChatController:viewJson");

//group anamneses
$Route->route("/panel/anamneses", "AnamneseController:index");
$Route->route("/panel/anamneses/register", "AnamneseController:register");
$Route->route("/panel/anamneses/edit/{anamneseId}", "AnamneseController:edit");
$Route->route("/panel/anamneses/print/{anamneseId}", "AnamneseController:print");
$Route->route("/panel/anamneses/delete/{anamneseId}", "AnamneseController:delete");

//group permissions
$Route->route("/panel/permissions", "PermissionController:index");
$Route->route("/panel/permissions/register", "PermissionController:register");
$Route->route("/panel/permissions/edit/{permissionId}", "PermissionController:edit");
$Route->route("/panel/permissions/delete/{permissionId}", "PermissionController:delete");

//group users
$Route->route("/panel/users", "UserController:index");
$Route->route("/panel/users/register", "UserController:register");
$Route->route("/panel/users/edit/{userId}", "UserController:edit");
$Route->route("/panel/users/delete/{userId}", "UserController:delete");

//group error
$Route->route("/oops/{errorCode}", "ErrorController:index");

$Route->execute();

if ($Route->error()) {
    $Route->redirect("/oops/{$Route->error()}");
}