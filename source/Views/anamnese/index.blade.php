@extends('templates.panel')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">Lista de Anamneses</h3>
                <a href="{{site()}}/panel/anamneses/register" class="btn btn-primary">Iniciar Anamnese</a>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    {!! getFlash() !!}
                    <table class="table dispĺay table-striped table-responsive-md" id="dataTable">
                        <thead class="table-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Pet</th>
                            <th scope="col">Data de cadastro</th>
                            <th scope="col">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($anamneses as $anamneseItem)
                            <tr>
                                <th scope="row">{{$anamneseItem->cdanamnese}}</th>
                                <td>{{$anamneseItem->nmparticipante}}</td>
                                <td>{{$anamneseItem->nmanimal}}</td>
                                <td>{{timestampsToBR($anamneseItem->dtcadastro)}}</td>
                                <td class="text-center">
                                    @if(hasPermission("edit-anamneses", false))
                                    <a href="{{site()}}/panel/anamneses/edit/{{$anamneseItem->cdanamnese}}" class="btn btn-default d-inline" data-toggle="tooltip" data-placement="top" title="Clique para editar"><i class="fas fa-pencil-alt"></i></a>
                                    @endif
                                    <a href="{{site()}}/panel/anamneses/print/{{$anamneseItem->cdanamnese}}"  target="_blank" class="btn btn-primary d-inline" data-toggle="tooltip" data-placement="top" title="Clique para imprimir"><i class="fas fa-file-pdf"></i></a>
                                    @if(hasPermission("delete-anamneses", false))
                                    <a href="{{site()}}/panel/anamneses/delete/{{$anamneseItem->cdanamnese}}" class="btn btn-danger d-inline j_delete" data-toggle="tooltip" data-placement="top" title="Clique para deletar"><i class="fas fa-trash-alt"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <div class="content">
                                <p class="alert alert-info"><span>Nenhuma anamnese cadastrada.</span></p>
                            </div>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection