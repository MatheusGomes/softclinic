@extends('templates.panel')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">{{isset($anamnese) ? "Editar Anamnese" : "Cadastro de Anamnese"}}</h3>
                <a href="{{site()}}/panel/anamneses" class="btn btn-default">Voltar para pesquisa</a>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body anamnese">
                    <form action="" class="form-register" method="POST">
                        {!! getFlash() !!}
                        <h4 class="title">Detalhes do Participante</h4>
                        @if(!isset($anamnese))
                        <div class="form-row">
                            <div class="form-group col-2">
                                <strong>Cliente:</strong> <br />
                                <select name="participante" class="form-control select-single" id="participante">
                                    @foreach($participants as $participantItem)
                                        <option value="{{$participantItem->cdparticipante}}">
                                            {{$participantItem->nmparticipante}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif
                        @if(isset($anamnese))
                        <div class="form-row">
                            <div class="col-2">
                                <strong>Cliente:</strong> <br />
                                <span class="text-gray-600 text-md">{{$participant->nmparticipante}}</span>
                            </div>
                            <div class="col-2">
                                <strong>CPF:</strong> <br />
                                <span class="text-gray-600 text-md">{{$participant->cpfparticipante}}</span>
                            </div>
                            <div class="col-2">
                                <strong>Telefone:</strong> <br />
                                <span class="text-gray-600 text-md">{{$participant->numcelular}}</span>
                            </div>
                            <div class="col">
                                <Strong>Endereço:</Strong> <br />
                                <span class="text-gray-600 text-md">{{$participant->nmlogradouro}}, {{$participant->nmbairro}}</span>
                            </div>
                        </div>
                        @endif
                        <hr />
                        <h4 class="title">Detalhes do Animal</h4>
                        <div class="form-row">
                            <div class="form-group col col-md-2">
                                <label for="nomeAnimal">Nome:</label>
                                <input type="text" name="nomeAnimal" id="nomeAnimal" class="form-control" placeholder="Informe o nome" value="{{$animal->nmanimal ?? ""}}" />
                            </div>
                            <div class="form-group col col-md-2">
                                <label for="sexoAnimal">Sexo do animal:</label>
                                <select name="sexoAnimal" id="sexoAnimal" class="form-control select-single">
                                    <option value="macho" {{( isset($anamnese) && $animal->sexo == "macho") ? "selected" : ""}}>Macho</option>
                                    <option value="femea" {{( isset($anamnese) && $animal->sexo == "femea") ? "selected" : ""}}>Fêmea</option>
                                </select>
                            </div>
                            <div class="form-group col col-md-2">
                                <label for="pesoAnimal">Peso:</label>
                                <div class="input-group mb-2">
                                    <input type="number" name="pesoAnimal" id="pesoAnimal" class="form-control" placeholder="Informe o peso" value="{{$animal->peso ?? ""}}" />
                                    <div class="input-group-append">
                                        <div class="input-group-text">kg</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col col-md-2">
                                <label for="idadeAnimal">Idade:</label>
                                <div class="input-group mb-2">
                                <input type="number" name="idadeAnimal" id="idadeAnimal" class="form-control" placeholder="Informe a idade" value="{{$animal->idade ?? ""}}" />
                                    <div class="input-group-append">
                                    <div class="input-group-text">Meses</div>
                                </div>
                            </div>
                            </div>
                            <div class="form-group col col-md-2">
                                <label for="especieAnimal">Espécie do Animal:</label>
                                <input type="text" name="especieAnimal" id="especieAnimal" class="form-control" placeholder="Qual a espécie?" value="{{$animal->especie ?? ""}}" />
                            </div>
                            <div class="form-group col col-md-2">
                                <label for="racaAnimal">Raça:</label>
                                <input type="text" name="racaAnimal" id="racaAnimal" class="form-control" placeholder="Informe a raça" value="{{$animal->raca ?? ""}}" />
                            </div>
                        </div>
                        <hr />
                        <ul class="nav nav-pills flex-column flex-sm-row mb-3" id="pills-tab" role="tablist">
                            <li class="flex-sm-fill text-sm-center nav-item">
                                <a class="nav-link active" id="anamnese-geral-tab" data-toggle="pill" href="#anamnese-geral" role="tab" aria-controls="anamnese-geral" aria-selected="true">
                                    Geral
                                </a>
                            </li>
                            <li class="flex-sm-fill text-sm-center nav-item">
                                <a class="nav-link" id="sistema-digestorio-tab" data-toggle="pill" href="#sistema-digestorio" role="tab" aria-controls="sistema-digestorio" aria-selected="false">
                                    Sistema Digestório
                                </a>
                            </li>
                            <li class="flex-sm-fill text-sm-center nav-item">
                                <a class="nav-link" id="cardio-respiratorio-tab" data-toggle="pill" href="#cardio-respiratorio" role="tab" aria-controls="cardio-respiratorio" aria-selected="false">
                                    Cardiorrespiratório
                                </a>
                            </li>
                            <li class="flex-sm-fill text-sm-center nav-item">
                                <a class="nav-link" id="urinario-tab" data-toggle="pill" href="#urinario" role="tab" aria-controls="urinario" aria-selected="false">
                                    Urinário
                                </a>
                            </li>
                            <li class="flex-sm-fill text-sm-center nav-item">
                                <a class="nav-link" id="genital-tab" data-toggle="pill" href="#genital" role="tab" aria-controls="genital" aria-selected="false">
                                    Genital
                                </a>
                            </li>
                            <li class="flex-sm-fill text-sm-center nav-item">
                                <a class="nav-link" id="nervoso-tab" data-toggle="pill" href="#nervoso" role="tab" aria-controls="nervoso" aria-selected="false">
                                    Nervoso
                                </a>
                            </li>
                            <li class="flex-sm-fill text-sm-center nav-item">
                                <a class="nav-link" id="esqueletico-tab" data-toggle="pill" href="#esqueletico" role="tab" aria-controls="esqueletico" aria-selected="false">
                                    Esquelético
                                </a>
                            </li>
                            <li class="flex-sm-fill text-sm-center nav-item">
                                <a class="nav-link" id="dermatologico-tab" data-toggle="pill" href="#dermatologico" role="tab" aria-controls="dermatologico" aria-selected="false">
                                    Dermatológico
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="anamnese-geral" role="tabpanel" aria-labelledby="anamnese-geral-tab">
                                <div class="form-row">
                                    <div class="form-group col col-md-6">
                                        <label for="queixaPrincipal">Queixa principal:</label>
                                        <input name="queixaPrincipal" id="queixaPrincipal" class="form-control" placeholder="O fez com que o proprietário procurasse atendimento veterinário?" value="{{$anamnese->informacoes->queixaprincipal ?? ""}}" required />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="tempoEvolucao">Tempo de evolução:</label>
                                        <input name="tempoEvolucao" id="tempoEvolucao" class="form-control" placeholder="A quanto tempo ele/ela notou os sinais relatados?" value="{{$anamnese->informacoes->tempoevolucao ?? ""}}" required />
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col col-md-6">
                                        <label for="tipoEvolucao">Tipo de evolução:</label>
                                        <input name="tipoEvolucao" id="tipoEvolucao" class="form-control" placeholder="Agudo, intermitente, crônico?" value="{{$anamnese->informacoes->tipoevolucao ?? ""}}" required />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="tipoTratamento">Tratamento:</label>
                                        <input name="tipoTratamento" id="tipoTratamento" class="form-control" placeholder="Qual o tratamento?" value="{{$anamnese->informacoes->tipotratamento ?? ""}}" required />
                                    </div>
                                </div>
                                <hr />
                                <h5 class="title">Manejo Geral e História Pregressa</h5>
                                <div class="form-row">
                                    <div class="form-group col col-md-6">
                                        <label for="alimentacao">Alimentação: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Investigue sobre o tipo de alimento que o paciente ingere, frequência de alimentação, armazenamento desse alimento e tipos de petiscos."><i class="far fa-question-circle"></i></span></label>
                                        <input name="alimentacao" id="alimentacao" class="form-control" placeholder="Investigue sobre o tipo de alimento que o paciente ingere." value="{{$anamnese->informacoes->alimentacao ?? ""}}" required />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="vacinacao">Vacinação: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Seja específico. Quando foi a última vacina? Qual tipo de vacina? Onde foi realizada?"><i class="far fa-question-circle"></i></span></label>
                                        <input name="vacinacao" id="vacinacao" class="form-control" placeholder="Investigue sobre a vacinação do paciente." value="{{$anamnese->informacoes->vacinacao ?? ""}}" required />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="vermifugacao">Vermifugação: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Quando foi a última vacinação? Qual produto utilizado? Qual protocolo instituído?"><i class="far fa-question-circle"></i></span></label>
                                        <input name="vermifugacao" id="vermifugacao" class="form-control" placeholder="Investiue sobre a vermifugação do paciente." value="{{$anamnese->informacoes->vermifugacao ?? ""}}" required />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="ambiente">Ambiente: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Local onde o paciente vive, tipo de piso do local onde passa a maior parte do tempo? Presença de matas fechadas próximo à residência? Contato com outros animais? Quais? Estão saudáveis?"><i class="far fa-question-circle"></i></span></label>
                                        <input name="ambiente" id="ambiente" class="form-control" placeholder="Investigue sobre o ambiente que o paciente vive." value="{{$anamnese->informacoes->ambiente ?? ""}}" required />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="acessoRua">Acesso à rua? Fugiu recentemente?</label>
                                        <input name="acessoRua" id="acessoRua" class="form-control" placeholder="O paciente tem acesso a rua ou fugiu recentemente?" value="{{$anamnese->informacoes->acessorua ?? ""}}" required />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="doencasAnteriores">Doenças anteriores: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Investigar sobre doenças anteriores e tratamento instituído."><i class="far fa-question-circle"></i></span></label>
                                        <input name="doencasAnteriores" id="doencasAnteriores" class="form-control" placeholder="O paciente teve doenças anteriores?" value="{{$anamnese->informacoes->doencasanteriores ?? ""}}" required />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="sistema-digestorio" role="tabpanel" aria-labelledby="sistema-digestorio-tab">
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="ingestaoAlimento" class="d-block">Ingestão de alimento:</label>
                                        <select name="ingestaoAlimento" class="select-single" id="ingestaoAlimento">
                                            <option value="anorexia" {{( isset($anamnese) && $anamnese->informacoes->ingestaoalimento == "anorexia") ? "selected" : ""}}>Anorexia (paciente não come nada)</option>
                                            <option value="hiporexia" {{( isset($anamnese) && $anamnese->informacoes->ingestaoalimento == "hiporexia") ? "selected" : ""}}>Hiporexia (paciente come pouco)</option>
                                            <option value="normorexia" {{( isset($anamnese) && $anamnese->informacoes->ingestaoalimento == "normorexia") ? "selected" : ""}}>Normorexia (paciente come normal)</option>
                                            <option value="polifagia" {{( isset($anamnese) && $anamnese->informacoes->ingestaoalimento == "polifagia") ? "selected" : ""}}>Polifagia (paciente come mais que o habitual)</option>
                                        </select>
                                    </div>
                                    <div class="form-group col">
                                        <label for="ingestaoAgua" class="d-block">Ingestão de água:</label>
                                        <select name="ingestaoAgua" class="select-single" id="ingestaoAgua">
                                            <option value="adpsia" {{( isset($anamnese) && $anamnese->informacoes->ingestaoagua == "adpsia") ? "selected" : ""}}>Adpsia (paciente não ingere água)</option>
                                            <option value="hipodipsia" {{( isset($anamnese) && $anamnese->informacoes->ingestaoagua == "hipodipsia") ? "selected" : ""}}>Hipodipsia (paciente ingere menos água que o habitual)</option>
                                            <option value="normodipsia" {{( isset($anamnese) && $anamnese->informacoes->ingestaoagua == "normodipsia") ? "selected" : ""}}>Normodipsia (Ingestão normal de água)</option>
                                            <option value="polidpsia" {{( isset($anamnese) && $anamnese->informacoes->ingestaoagua == "polidpsia") ? "selected" : ""}}>Polidpsia (paciente ingere mais água que o habitual)</option>
                                        </select>
                                    </div>
                                    <div class="form-group col">
                                        <label for="defecacao" class="d-block">Defecação:</label>
                                        <select name="defecacao" class="select-single" id="defecacao">
                                            <option value="normoquezia" {{( isset($anamnese) && $anamnese->informacoes->defecacao == "normoquezia") ? "selected" : ""}}>Normoquezia (Defeca normalmente)</option>
                                            <option value="tenesmo" {{( isset($anamnese) && $anamnese->informacoes->defecacao == "tenesmo") ? "selected" : ""}}>Tenesmo (Dificuldade para defecar)</option>
                                            <option value="diarreia" {{( isset($anamnese) && $anamnese->informacoes->defecacao == "diarreia") ? "selected" : ""}}>Diarréia (Fezes amolecidas)</option>
                                            <option value="melena" {{( isset($anamnese) && $anamnese->informacoes->defecacao == "melena") ? "selected" : ""}}>Melena (Sangue digerido nas fezes - Remete a uma afecção de TGI superior)</option>
                                            <option value="hematoquezia" {{( isset($anamnese) && $anamnese->informacoes->defecacao == "hematoquezia") ? "selected" : ""}}>Hematoquezia (Sangue digerido nas fezes - Remete a uma afecção de TGI superior)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="abdomen" class="d-block">Abdômen:</label>
                                        <select name="abdomen" class="select-single" id="abdomen">
                                            <option value="abdominalgia" {{( isset($anamnese) && $anamnese->informacoes->abdomen == "abdominalgia") ? "selected" : ""}}>Abdominalgia (Dor abdominal)</option>
                                            <option value="organomegalia" {{( isset($anamnese) && $anamnese->informacoes->abdomen == "organomegalia") ? "selected" : ""}}>Organomegalia (Aumento de algum órgão à palpação)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="observacaoDigestorio">Observações adicionais:</label>
                                        <textarea name="observacaoDigestorio" id="observacaoDigestorio" class="form-control" rows="3">{{$anamnese->informacoes->observacaodigestorio ?? ""}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="cardio-respiratorio" role="tabpanel" aria-labelledby="cardio-respiratorio-tab">
                                <div class="form-row">
                                    <div class="form-group col col-md-6">
                                        <label for="cansacoFacil">Apresenta cansaço fácil?</label>
                                        <input type="text" name="cansacoFacil" id="cansacoFacil"  class="form-control" placeholder="Estava acostumado a correr e já não o faz mais?" value="{{$anamnese->informacoes->cansacofacil ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="apresentaCianose">Apresentou algum episódio de cianose?</label>
                                        <input type="text" name="apresentaCianose" id="apresentaCianose" class="form-control" placeholder="língua de coloração roxeado/azulada?" value="{{$anamnese->informacoes->apresentacianose ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="tosseAnimal">Tosse? Qual a frequência? É tosse seca ou com expectoração (produtiva)? <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="alguns animais com problema cardíaco apresentam tosse seca que piora à noite em virtude do decúbito"><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="tosseAnimal" id="tosseAnimal" class="form-control" placeholder="Investigue sobre a tosse do paciente." value="{{$anamnese->informacoes->tosseanimal ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="tipoExpectoracao">Qual o aspecto da expectoração (cor, odor, volume)?</label>
                                        <input type="text" name="tipoExpectoracao" id="tipoExpectoracao" class="form-control" placeholder="Investigue sobre o tipo da expectoração do paciente." value="{{$anamnese->informacoes->tipoexpectoracao ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="apresentaEspirros">O animal tem espirros? Com que frequência?</label>
                                        <input type="text" name="apresentaEspirros" id="apresentaEspirros" class="form-control" placeholder="Investigue sobre os espirros do paciente." value="{{$anamnese->informacoes->apresentaespirros ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="secrecaoNasal">Secreção nasal? Serosa, purulenta, sanguinolenta (Epistaxe)</label>
                                        <input type="text" name="secrecaoNasal" id="secrecaoNasal" class="form-control" placeholder="Investigue sobre a secreção nasal do paciente." value="{{$anamnese->informacoes->secrecaonasal ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="inchacoAnimal">Observou edema ou inchaço em alguma parte do corpo? <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Época que apareceu; evolução; região que predomina"><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="inchacoAnimal" id="inchacoAnimal" class="form-control" placeholder="Investigue sobre os inchaços ou edemas do paciente." value="{{$anamnese->informacoes->inchacoanimal ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="animalFraco">O animal lhe parece fraco?</label>
                                        <input type="text" name="animalFraco" id="animalFraco" class="form-control" placeholder="Investigue sobre as fraquezas do animal." value="{{$anamnese->informacoes->animalfraco ?? ""}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="urinario" role="tabpanel" aria-labelledby="urinario-tab">
                                <div class="form-row">
                                    <div class="form-group col col-md-6">
                                        <label for="animalUrinando">O animal está urinando?</label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="animalUrinando1" name="animalUrinando" class="custom-control-input" value="sim" {{( isset($anamnese) && isset($anamnese->informacoes->animalurinando) == "sim") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="animalUrinando1">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="animalUrinando2" name="animalUrinando" class="custom-control-input" value="nao" {{( isset($anamnese) && isset($anamnese->informacoes->animalurinando) == "nao") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="animalUrinando2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="urinaComFormigas">Onde o animal urina aparecem formigas?</label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="urinaComFormigas1" name="urinaComFormigas" class="custom-control-input" value="sim" {{( isset($anamnese) && isset($anamnese->informacoes->urinacomformigas) == "sim") ? "checked" : ""}} />
                                                <label class="custom-control-label" for="urinaComFormigas1">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="urinaComFormigas2" name="urinaComFormigas" class="custom-control-input" value="nao" {{( isset($anamnese) && isset($anamnese->informacoes->urinacomformigas) == "nao") ? "checked" : ""}} />
                                                <label class="custom-control-label" for="urinaComFormigas2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="frequenciaUrina">Qual a frequência?</label>
                                        <input type="text" name="frequenciaUrina" id="frequenciaUrina" class="form-control" value="{{$anamnese->informacoes->frequenciaurina ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="coloracaoUrina">Qual a coloração da urina?</label>
                                        <input type="text" name="coloracaoUrina" id="coloracaoUrina" class="form-control" value="{{$anamnese->informacoes->coloracaourina ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="odorUrina">Qual o odor?</label>
                                        <input type="text" name="odorUrina" id="odorUrina" class="form-control" value="{{$anamnese->informacoes->odorurina ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="dorUrina">Aparentemente o animal sente dor quando urina? <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="posição à micção, gemidos, emissão lenta e vagarosa, gotejamento"><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="dorUrina" id="dorUrina" class="form-control" value="{{$anamnese->informacoes->dorurina ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="tipoUrina" class="d-block">Tipo de urina:</label>
                                        <select name="tipoUrina" class="select-single" id="tipoUrina">
                                            <option value="normouria" {{( isset($anamnese) && $anamnese->informacoes->urinacomformigas == "normouria") ? "selected" : ""}}>Normoúria (paciente urina normalmente)</option>
                                            <option value="anuria"  {{( isset($anamnese) && $anamnese->informacoes->urinacomformigas == "anuria") ? "selected" : ""}}>Anúria (paciente não produz urina)</option>
                                            <option value="oliguria" {{( isset($anamnese) && $anamnese->informacoes->urinacomformigas == "oliguria") ? "selected" : ""}}>Oligúria (paciente urina em pouca quantidade)</option>
                                            <option value="poliuria" {{( isset($anamnese) && $anamnese->informacoes->urinacomformigas == "poliuria") ? "selected" : ""}}>Poliúria (paciente urina em grande quantidade)</option>
                                            <option value="disuria" {{( isset($anamnese) && $anamnese->informacoes->urinacomformigas == "disuria") ? "selected" : ""}}>Disúria (Dor ou ardor ao urinar)</option>
                                            <option value="estranguria" {{( isset($anamnese) && $anamnese->informacoes->urinacomformigas == "estranguria") ? "selected" : ""}}>Estrangúria (eliminação lenta e dolorosa de urina)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="genital" role="tabpanel" aria-labelledby="genital-tab">
                                <div class="form-row">
                                    <div class="form-group col col-md-6">
                                        <label for="animalCastrado">Castrado(a)? <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Às vezes precisa ser específico: Seu cão fez alguma cirurgia para tirar o útero ou os testículos?"><i class="far fa-question-circle"></i></span></label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="animalCastrado1" name="animalCastrado" class="custom-control-input" value="sim" {{( isset($anamnese) && isset($anamnese->informacoes->animalcastrado) == "sim") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="animalCastrado1">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="animalCastrado2" name="animalCastrado" class="custom-control-input" value="nao" {{( isset($anamnese) && isset($anamnese->informacoes->animalcastrado) == "nao") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="animalCastrado2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Fêmea -->
                                    <div class="form-group col col-md-6 femea">
                                        <label for="jaPariu">O animal já pariu alguma vez? <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="O parto foi normal? Quando foi o último cio? Percebeu alguma secreção vaginal? Percebeu algum nódulo vaginal ou em mamas? Já tomou “vacina anti-cio”? Quando?"><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="jaPariu" id="jaPariu" placeholder="Se sim, investigue sobre o parto do paciente." class="form-control" value="{{$anamnese->informacoes->japariu ?? ""}}"/>
                                    </div>
                                    <div class="form-group col col-md-6 femea">
                                        <label for="secrecaoVaginal">Secreção vaginal: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Descrever coloração e consistência."><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="secrecaoVaginal" id="secrecaoVaginal" placeholder="Descrever coloração e consistência." class="form-control" value="{{$anamnese->informacoes->secrecaovaginal ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6 femea">
                                        <label for="presencaCio">Presença de cio: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Descrever quando foi o último cio e quanto tempo durou."><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="presencaCio" id="presencaCio" placeholder="Descrever quando foi o último cio e quanto tempo durou." class="form-control" value="{{$anamnese->informacoes->presencacio ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6 femea">
                                        <label for="anticoncepcional">Aplicação de anticoncepcional: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Dose usada, período, frequência."><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="anticoncepcional" id="anticoncepcional" placeholder="Dose usada, período, frequência" class="form-control" value="{{$anamnese->informacoes->anticoncepcional ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6 femea">
                                        <label for="sangramentoVaginal">Presença massa ou sangramento vaginal: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Descrever seu formato, período em que apareceu e se cruzou recentemente."><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="sangramentoVaginal" id="sangramentoVaginal" placeholder="Caso sim, investigue sobre o acontecido." class="form-control" value="{{$anamnese->informacoes->sangramentovaginal ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6 femea">
                                        <label for="noduloMamario">Nódulo mamário: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Questionar período em que apareceu, evolução, crescimento progressivo ou não, consistência."><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="noduloMamario" id="noduloMamario" class="form-control" value="{{$anamnese->informacoes->nodulomamario ?? ""}}" />
                                    </div>
                                    <!-- Macho -->
                                    <div class="form-group col col-md-6 macho">
                                        <label for="testiculos">Testículos: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Tamanho, sensibilidade, localização"><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="testiculos" id="testiculos" placeholder="Tamanho, sensibilidade, localização" class="form-control" value="{{$anamnese->informacoes->testiculos ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6 macho">
                                        <label for="secrecaoPeniana">Secreção peniana: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Descrever coloração e consistência."><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="secrecaoPeniana" id="secrecaoPeniana" placeholder="Descrever coloração e consistência." class="form-control" value="{{$anamnese->informacoes->secrecaopeniana ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6 macho">
                                        <label for="sangramentoPeniano">Presença massa ou sangramento peniano: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Descrever seu formato, período em que apareceu e se cruzou com alguma fêmea."><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="sangramentoPeniano" id="sangramentoPeniano" placeholder="Descrever seu formato, período em que apareceu e se cruzou com alguma fêmea." class="form-control" value="{{$anamnese->informacoes->sangramentopeniano ?? ""}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nervoso" role="tabpanel" aria-labelledby="nervoso-tab">
                                <div class="form-row">
                                    <div class="form-group col col-md-6">
                                        <label for="comportamento">Apresentou mudanças de comportamento (agressividade)?</label>
                                        <input type="text" name="comportamento" id="comportamento" placeholder="Investigue sobre o comportamento do paciente." class="form-control" value="{{$anamnese->informacoes->comportamento ?? ""}}" />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="convulsoes">Apresentou convulsões?</label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="convulsoes1" name="convulsoes" class="custom-control-input" value="sim" {{( isset($anamnese) && $anamnese->informacoes->convulsoes == "sim") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="convulsoes1">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="convulsoes2" name="convulsoes" class="custom-control-input" value="nao" {{( isset($anamnese) && $anamnese->informacoes->convulsoes == "nao") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="convulsoes2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="dificuldadesAndar">Apresenta dificuldade para andar? Tem dificuldade para subir escadas?</label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="dificuldadesAndar1" name="dificuldadesAndar" class="custom-control-input" value="sim" {{( isset($anamnese) && isset($anamnese->informacoes->dificuldadesandar) == "sim") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="dificuldadesAndar1">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="dificuldadesAndar2" name="dificuldadesAndar" class="custom-control-input" value="nao" {{( isset($anamnese) && isset($anamnese->informacoes->dificuldadesandar) == "nao") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="dificuldadesAndar2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="andaEmCirculos">Anda em círculos?</label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="andaEmCirculos1" name="andaEmCirculos" class="custom-control-input" value="sim" {{( isset($anamnese) && isset($anamnese->informacoes->andaemcirculos) == "sim") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="andaEmCirculos1">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="andaEmCirculos2" name="andaEmCirculos" class="custom-control-input" value="nao" {{( isset($anamnese) && isset($anamnese->informacoes->andaemcirculos) == "nao") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="andaEmCirculos2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="inclunacaoCabeca">Apresenta inclinação de cabeça?</label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="inclinacaoCabeca1" name="inclinacaoCabeca" class="custom-control-input" value="sim" {{( isset($anamnese) && isset($anamnese->informacoes->inclinacaocabeca) == "sim") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="inclinacaoCabeca2">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="inclinacaoCabeca2" name="inclinacaoCabeca" class="custom-control-input" value="nao" {{( isset($anamnese) && isset($anamnese->informacoes->inclinacaocabeca) == "nao") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="inclinacaoCabeca2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="apresentaTropecos">Apresenta tropeços ou quedas quando caminha?</label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="apresentaTropecos1" name="apresentaTropecos" class="custom-control-input" value="sim" {{( isset($anamnese) && isset($anamnese->informacoes->apresentatropecos) == "sim") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="apresentaTropecos1">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="apresentaTropecos2" name="apresentaTropecos" class="custom-control-input" value="nao" {{( isset($anamnese) && isset($anamnese->informacoes->apresentatropecos) == "nao") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="apresentaTropecos2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="atendeChamados">Ele atende quando chama?</label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="atendeChamados1" name="atendeChamados" class="custom-control-input" value="sim" {{( isset($anamnese) && isset($anamnese->informacoes->atendechamados) == "sim") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="atendeChamados1">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="atendeChamados2" name="atendeChamados" class="custom-control-input" value="nao" {{( isset($anamnese) && isset($anamnese->informacoes->atendechamados) == "nao") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="atendeChamados2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="ficaDeitado">Fica o tempo todo deitado?</label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="ficaDeitado1" name="ficaDeitado" class="custom-control-input" value="sim" {{( isset($anamnese) && isset($anamnese->informacoes->ficadeitado) == "sim") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="ficaDeitado1">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="ficaDeitado2" name="ficaDeitado" class="custom-control-input" value="nao" {{( isset($anamnese) && isset($anamnese->informacoes->ficadeitado) == "nao") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="ficaDeitado2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="respondeEstimulos">Responde à estímulos?</label>
                                        <div class="d-block">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="respondeEstimulos1" name="respondeEstimulos" class="custom-control-input" value="sim" {{( isset($anamnese) && isset($anamnese->informacoes->respondeestimulos) == "sim") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="respondeEstimulos1">Sim</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="respondeEstimulos2" name="respondeEstimulos" class="custom-control-input" value="nao" {{( isset($anamnese) && isset($anamnese->informacoes->respondeestimulos) == "nao") ? "checked" : ""}}>
                                                <label class="custom-control-label" for="respondeEstimulos2">Não</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="esqueletico" role="tabpanel" aria-labelledby="esqueletico-tab">
                                <div class="form-row">
                                    <div class="form-group col col-md-6">
                                        <label for="animalMancando">O animal está mancando? <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="De que membro? Há quanto tempo? à Descrever a claudicação como unilateral ou bilateral, intermitente ou contínua."><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="animalMancando" id="animalMancando" placeholder="Qual membro? Investigue a respeito." class="form-control" value="{{$anamnese->informacoes->animalmancando ?? ""}}"  />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="traumasRecentes">Teve algum histórico de trauma recente? <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Quedas, atropelamentos, brigas, fugas, etc"><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="traumasRecentes" id="traumasRecentes" placeholder="Investigue sobre tais traumas." class="form-control" value="{{$anamnese->informacoes->traumasrecentes ?? ""}}"  />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="senteDores">Sente alguma dor ao andar ou quando manipula os membros?</label>
                                        <input type="text" name="senteDores" id="senteDores" placeholder="Investiue sobre tais dores." class="form-control" value="{{$anamnese->informacoes->sentedores ?? ""}}"  />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="animalAndando">Animal que não está andando: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Há quanto tempo não anda? Trauma? Agudo ou crônico? Se arrasta ou apoia outros membros?"><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="animalAndando" id="animalAndando" placeholder="Caso não esteja andando, investigue a respeito." class="form-control" value="{{$anamnese->informacoes->animalandando ?? ""}}"  />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="dermatologico" role="tabpanel" aria-labelledby="dermatologico-tab">
                                <div class="form-row">
                                    <div class="form-group col col-md-6">
                                        <label for="animalSeConcando">O animal se coça muito? <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="O prurido é intenso (graduar)? Chega a se automutilar?"><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="animalSeConcando" id="animalSeConcando" placeholder="Se sim, investigue a respeito." class="form-control" value="{{$anamnese->informacoes->animalseconcando ?? ""}}"  />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="maneiosDeCabeca">Apresenta meneios de cabeça ou bate a pata na orelha o tempo todo (otite)?</label>
                                        <input type="text" name="maneiosDeCabeca" id="maneiosDeCabeca" placeholder="Se sim, investigue a respeito." class="form-control" value="{{$anamnese->informacoes->maneiosdecabeca ?? ""}}"  />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="quedaDePelos">Queda de pêlos? <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Está apresentando queda de pêlos? Há quanto tempo? Existe algum período em que é mais intenso? Apresenta alguma área específica com maior queda de pelos."><i class="far fa-question-circle"></i></span></label>
                                        <input type="text" name="quedaDePelos" id="quedaDePelos" placeholder="Se sim, investigue a respeito." class="form-control" value="{{$anamnese->informacoes->quedadepelos ?? ""}}"  />
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label for="teveCarrapatos">Tem ou teve carrapatos e pulgas recentemente? (Usou alguma medicação?)</label>
                                        <input type="text" name="teveCarrapatos" id="teveCarrapatos" placeholder="Investigue a respeito para obter informações precisas." class="form-control" value="{{$anamnese->informacoes->tevecarrapatos ?? ""}}"  />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">{{isset($anamnese) ? "Salvar" : "Cadastrar"}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection