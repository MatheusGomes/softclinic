@extends('templates.auth')
@section('content')
<div class="container-fluid login">
    <div class="row align-items-center">
        <div class="col-12 col-md-6 order-2 order-md-1">
            <div class="logo text-center">
                <img src="{{asset("img/logo.png")}}" alt="Sofclinic" />
            </div>
            <form action="" class="form-login" method="POST">
                {!! getFlash() !!}
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                    </div>
                    <input type="email" name="email" class="form-control" placeholder="Seu email" required />
                </div>
                <div class="input-group mb-0">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                    </div>
                    <input type="password" name="passwd" class="form-control" placeholder="Sua senha" required />
                </div>
                <div class="mr-3 my-0 d-flex justify-content-end">
                    <a class="text-black-50 font-italic" href="{{site()}}/recovery">Esqueci a senha</a>
                </div>
                <div class="custom-control custom-switch mb-3">
                    <input type="checkbox" class="custom-control-input" id="manterconectado">
                    <label class="custom-control-label text-black-50" for="manterconectado">Lembrar senha</label>
                </div>

                <div class="input-group">
                    <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                </div>
                <hr class="mt-5 mb-2">
                <div class="mx-auto text-center text-black-50">
                    <span>Não possui conta?</span>&nbsp;&nbsp;<a class="font-italic text-secondary font-weight-bold text-black-100  " href="{{site()}}/register">Criar conta</a>
                </div>
            </form>
        </div>
        <div class="col-12 col-md-6 order-1  order-md-2 background-login text-center">
            <h2>Gerencie seus atendimentos
                de forma rápida e fácil!</h2>
            <h5>Bem vindo ao Soft<span>Clinc</span>!</h5>
        </div>
    </div>
</div>
@endsection