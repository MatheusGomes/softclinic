@extends('templates.auth')
@section('content')
<div class="container-fluid login">
    <div class="row align-items-center justify-content-center">
        <div class="col-12 col-sm-10 col-md-6 order-2 order-md-1">
            <div class="logo text-center">
                <img src="{{asset("img/logo.png")}}" alt="Sofclin" />
            </div>
            <form action="" class="form-register" method="POST">
                <div class="col-8 mx-auto">
                    <ul class="wizard-list">
                        <li {!! $step == 1 ? 'class="active"' : '' !!}><i class="fas fa-user"></i> <span>Usuário</span></li>
                        <li {!! $step == 2 ? 'class="active"' : '' !!}><i class="far fa-address-card"></i> <span>Pessoal</span></li>
                    </ul>
                    {!! getFlash() !!}
                    @if($step == 1)
                    <div class="form-row">
                        <div class="form-group col">
                            <input type="email" name="email" class="form-control" placeholder="Endereço de e-mail" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col col-md-6">
                            <input type="password" name="senha" class="form-control" placeholder="Senha" />
                        </div>
                        <div class="form-group col col-md-6">
                            <input type="password" name="confirmaSenha" class="form-control" placeholder="Confirmar Senha" />
                        </div>
                    </div>
                    @else
                        <div class="form-row">
                            <input type="hidden" name="step" value="2" />
                            <div class="form-group col">
                                <input type="text" name="nome" class="form-control my-0" placeholder="Nome Completo" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-3">
                                <input name="cep" type="text" id="cep" value="" size="10" maxlength="9" class="form-control" placeholder="CEP">
                            </div>
                            <div class="form-group col-6">
                                <input name="rua" type="text" id="rua" size="60" class="form-control" placeholder="Rua">
                            </div>
                            <div class="form-group col-3">
                                <input name="bairro" type="text" id="bairro" size="40" class="form-control" placeholder="Bairro">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <select name="cidade" class="form-control select-single" id="cidade" required>
                                    @foreach($listCity as $cityItem)
                                        <option value="{{$cityItem->cdcidade}}" {{( isset($participant) && $participant->cdcidade == $cityItem->cdcidade) ? "selected" : ""}}>
                                            {{$cityItem->nmcidade}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" name="cpf" class="form-control" placeholder="CPF" size="14">
                            </div>
                            <div class="form-group col">
                                <input type="text" name="telefone" id="telefone" maxlength="15" class="form-control" placeholder="Telefone">
                            </div>
                            <div class="form-group col-4">
                                <select name="sexo" class="form-control select-single" id="sexo" required>
                                    @foreach($listSex as $listSexParticipantItem)
                                        <option value="{{$listSexParticipantItem->cdsexo}}" {{( isset($participant) && $participant->cdsexo == $listSexParticipantItem->cdsexo) ? "selected" : ""}}>
                                            {{$listSexParticipantItem->nmsexo}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <button type="submit" class="btn btn-primary">{!! $step == 1 ? 'Próximo' : 'Finalizar' !!}</button>
                    <hr class="mt-5 mb-2">
                    <div class="mx-auto text-center text-black-50">
                        <span>Já possui uma conta?</span>&nbsp;&nbsp;<a class="font-italic text-secondary font-weight-bold text-black-100  " href="{{site()}}">Fazer login</a>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-12 col-md-6 order-1  order-md-2 background-login text-center">
            <h2>Crie sua conta rápido e fácil!</h2>
            <h5>Soft<span>Clinc</span>!</h5>
        </div>
    </div>
</div>
@endsection