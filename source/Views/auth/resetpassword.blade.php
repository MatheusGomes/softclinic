@extends('templates.auth')

@section('content')
<div class="container-fluid login">
    <div class="row align-items-center">
        <div class="col-12 col-md-6 order-2 order-md-1">
            <div class="logo text-center mb-5">
                <img src="{{asset("img/logo.png")}}" alt="Sofclinic" />
            </div>
            <div class="container shadow col col-md-12 col-lg-10">
                <div class="col pt-3">
                    <a class="text-dark text-decoration-none" href="{{site()}}/"> <i class="fas fa-reply-all"></i> Login</a>
                </div>
                <div class="col text-center">
                    <h4>Alteração de senha</h4>
                </div>
                <form action="" class="form-row col-10 mx-auto mt-3" method="POST">
                    <div class="col">
                        {!! getFlash() !!}
                    </div>
                    <div class="input-group mb-5">
                        <input type="password" name="senha" class="form-control" placeholder="Informe sua senha" required />
                        <input type="password" name="confirmaSenha" class="form-control" placeholder="Confirme a sua senha" required />
                        <div class="input-group-append">
                            <button class="btn bg-primary"  type="submit">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
            <hr class=" col col-md-9 mt-5">
            <div class="row text-center">
                <div class="col">
                    Não possui cadastro? &nbsp;<a class="font-italic text-secondary font-weight-bold text-decoration-none" href="{{site()}}/register">Criar conta</a>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 order-1  order-md-2 background-login text-center">
            <h2>Recuperação de Senha</h2>
            <h5> Soft<span>Clinc</span>!</h5>
        </div>
    </div>
</div>
@endsection