@extends('templates.panel')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">Lista de Chats</h3>
                <a href="{{site()}}/panel/chats/register" class="btn btn-primary">Iniciar um novo chat</a>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    {!! getFlash() !!}
                    <table class="table dispĺay table-striped table-responsive-md" id="dataTable">
                        <thead class="table-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Remetente</th>
                            <th scope="col">Destinatário</th>
                            <th scope="col">Data de envio</th>
                            <th scope="col">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($chats as $chat)
                            <tr>
                                <th scope="row">{{$chat->cdmensagem}}</th>
                                <td>{{$chat->nomeremetente}}</td>
                                <td>{{$chat->nomedestinatario}}</td>
                                <td>{{timestampsToBR($chat->dtenvio)}}</td>
                                <td class="text-center">
                                    <a href="{{site()}}/panel/chats/view/{{$chat->cdmensagem}}" class="btn btn-default d-inline" data-toggle="tooltip" data-placement="top" title="Clique para visualizar"><i class="fas fa-eye"></i> Ver conversa</a>
                                    {{--<a href="{{site()}}/panel/chats/delete/{{$chat->cdmensagem}}" class="btn btn-danger d-inline j_delete" data-toggle="tooltip" data-placement="top" title="Clique para deletar"><i class="fas fa-trash-alt"></i></a>--}}
                                </td>
                            </tr>
                        @empty
                            <div class="content">
                                <p class="alert alert-info"><span>Nenhuma mensagem recebida</span></p>
                            </div>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection