@extends('templates.panel')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">{{isset($user) ? "Editar Usuário" : "Cadastro de Usuário"}}</h3>
                <a href="{{site()}}/panel/users" class="btn btn-default">Voltar para pesquisa</a>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form action="" class="form-register" method="POST">
                        {!! getFlash() !!}
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="destinatario">Destinatário:</label>
                                <select name="destinatario" class="form-control select-single"  id="destinatario" required>
                                    @foreach($users as $usersItem)
                                        <option value="{{$usersItem->cdusuario}}">
                                            {{$usersItem->nmparticipante}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label>Mensagem:</label>
                                <textarea name="content" rows="4" class="form-control ilustre_mce_basic" placeholder="Digite sua mensagem"></textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success">{{isset($user) ? "Salvar" : "Cadastrar"}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection