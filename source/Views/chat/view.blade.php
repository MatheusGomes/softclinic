@extends('templates.panel')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">Conversa com {{ ($chat->cdusuario_remetente == \Source\Core\Session::get("auth")["cdusuario"]) ? $chat->nomedestinatario : $chat->nomeremetente }}</h3>
                <a href="{{site()}}/panel/chats" class="btn btn-default">Voltar para pesquisa</a>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="chat-page-content" id="chat">
                                <div class="chat-page-content-body">
                                    <div class="chat-messenger">
                                        <div class="chat-messages chat-messenger__messages">
                                            <div class="chat-body">
                                                <ul class="chat-items">
                                                    <li class="chat-item {{ ($chat->cdusuario_remetente === \Source\Core\Session::get("auth")["cdusuario"]) ? 'chat-from':'chat-self' }}">
                                                        <div class="chat-body">
                                                            <div class="chat-header">
                                                                <span class="chat-name">{{ $chat->nomeremetente }}</span>
                                                                <span class="chat-datetime" data-datetime="{{$chat->dtenvio}}">{{timestampsToBR($chat->dtenvio)}}</span>
                                                            </div>
                                                            <div class="chat-message">{{$chat->conteudo}}</div>
                                                        </div>
                                                    </li>
                                                    @foreach($chat->responses as $messages)
                                                        <li class="chat-item {{ ($messages->cdremetente === \Source\Core\Session::get("auth")["cdusuario"]) ? 'chat-from':'chat-self' }}">
                                                            <div class="chat-body">
                                                                <div class="chat-header">
                                                                    <span class="chat-name">{{ ($messages->cdremetente === $chat->cdusuario_remetente) ? $chat->nomeremetente : $chat->nomedestinatario }}</span>
                                                                    <span class="chat-datetime" data-datetime="{{$messages->dtenvio}}">{{timestampsToBR($messages->dtenvio)}}</span>
                                                                </div>
                                                                <div class="chat-message">{{$messages->conteudo}}</div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <form action="{{site()}}/panel/chats/view/{{$chat->cdmensagem}}" id="js-form"  data-reset="true" class="form-register" method="POST" enctype="multipart/form-data">
                                {!! getFlash() !!}
                                <div class="form-group">
                                    <label>Responder</label>
                                    <textarea name="content" rows="4" class="form-control ilustre_mce_basic" placeholder="Digite sua mensagem"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success"><i class="ti-check"></i> Responder</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection