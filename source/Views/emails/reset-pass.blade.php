<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recuperação de senha</title>
</head>
<body style="width: 70%;height: 70%;background-color: #E8E8E8;color: black;font-family: arial,serif; border: 1px solid black; padding: 5px; margin-left: 13%;">
<main style="display: flex;flex-direction: row;flex-wrap: wrap;">
    <div style="width: 90%; padding: 15px;">
        <span>Nós recebemos uma solicitação de alteração de senha para o seu email</span>
        <br>
        <span>{{$email}}</span>
        <br>
        <br>
        <span>
            Clique <a href="{{$url}}" style="color: blue;text-decoration: underline;">Aqui</a> para criar uma nova senha.
        </span>
        <br>
        <br>
        <h2>Se você não solicitou a mudança de senha, desconsidere este email.</h2>
        <br>
        <div style="text-align: center;"><h3>Por favor não responder este e-mail, esta é uma mensagem automática.</h3></div>
    </div>
</main>
<hr>
<footer style="display: flex;flex-direction: row;flex-wrap: wrap; text-align: center;">
    <div style="width: 90%;">
        <h4><span style="font-size: 20px;">SoftClinic</span> Software para gestão de clínicas veterinárias.</h4>
        <h4>Todos os direitos reservados Softclinic - 2020.</h4>
    </div>
</footer>
</body>
</html>