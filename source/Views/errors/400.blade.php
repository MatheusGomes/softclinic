@extends('templates.auth')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col justify-content-center text-center">
            <h5 class="d-flex justify-content-start mt-3">
                <a class="btn btn-erro text-decoration-none" href="{{site()}}/">
                    <i class="fas fa-reply-all"></i> Ir para o Início
                </a>
            </h5>
            <div class="col erro400">
                <img class="img-fluid my-5 img2 col-md-5" src="{{asset("img/errors/400.png")}}" alt="erro400" />
                <img class="img-fluid my-5 img1 col-md-5" src="{{asset("img/errors/gato.png")}}" alt="erro400" />
            </div>
        </div>
    </div>
</div>
@endsection