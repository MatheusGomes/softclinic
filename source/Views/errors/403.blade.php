@extends('templates.auth')
@section('content')
    <div class="container-fluid bg-erro403" style="height: 100%;">
        <div class="row d-flex">
            <div class="col-12 justify-content-center text-center">
                <h5 class="d-flex justify-content-start mt-3 mb-0">
                    <a class="btn btn-erro text-decoration-none" href="{{site()}}/">
                        <i class="fas fa-reply-all"></i> Ir para o Início
                    </a>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-6 text-center">
                <img class="img-fluid my-5" src="{{asset("img/errors/403.png")}}" alt="erro403 acesso negado" />
            </div>
            <div class="col-4 d-flex align-self-center align-items-start flex-column bd-highlight ">
                <div class="bd-highlight erro403"><h1>403</h1></div>
                <div class="bd-highlight erro403"><h3 class="display-4">Acesso Negado</h3></div>
            </div>
        </div>
    </div>
@endsection