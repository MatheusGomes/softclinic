@extends('templates.auth')
@section('content')
<div class="container-fluid bg-erro404">
    <div class="row">
        <div class="col justify-content-center text-center">
            <h5 class="d-flex justify-content-start mt-3">
                <a class="btn btn-erro text-decoration-none" href="{{site()}}/">
                    <i class="fas fa-reply-all"></i> Ir para o Início
                </a>
            </h5>
            <div class="col">
                <img class="img-fluid my-5" src="{{asset("img/errors/404.png")}}" alt="erro404" />
                <img class="img-fluid my-5" src="{{asset("img/errors/404.gif")}}" alt="erro404" />
            </div>
        </div>
    </div>
</div>
@endsection