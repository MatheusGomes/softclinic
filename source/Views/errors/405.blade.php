@extends('templates.auth')
@section('content')
<div class="container-fluid bg-erro405" style="height: 100%;">
    <div class="row">
        <div class="col text-center">
            <h5 class="d-flex justify-content-start mt-3">
                <a class="btn btn-erro text-decoration-none" href="{{site()}}/">
                    <i class="fas fa-reply-all"></i> Ir para o Início
                </a>
            </h5>
            <div class="col d-flex flex-wrap align-self-center flex-row bd-highlight erro405">
                <h1 class="col-12 erro">Error 405</h1>
                <h3 class="col-12 mb-5">Método não permitido</h3>
                <hr class="col-8 bg-light">
            </div>
        </div>
    </div>
</div>
@endsection