@extends('templates.auth')
@section('content')
<div class="container-fluid bg-erro500">
    <div class="row">
        <div class="col text-center">
            <img class="erro500" src="{{asset("img/errors/500.jpg")}}" alt="<"/>
            <h5 class="d-flex justify-content-center mt-3">
                <a class="btn btn-erro text-decoration-none" href="{{site()}}/">
                    <i class="fas fa-reply-all"></i> Ir para o Início
                </a>
            </h5>
        </div>
    </div>
</div>
@endsection