<nav class="navbar navbar-expand-lg">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarAside" aria-controls="navbarAside" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarAside">
        <ul class="navbar-nav">
            @if(hasPermission("view-panel", false))
            <li class="nav-item active">
                <a class="nav-link" href="{{site()}}/panel"><i class="fas fa-chart-line"></i> Dashboard <span class="sr-only">(current)</span></a>
            </li>
            @endif
            @if(hasPermission("list-participants", false))
            <li class="nav-item">
                <a class="nav-link" href="{{site()}}/panel/participants"><i class="fas fa-user-tie"></i> Participantes</a>
            </li>
            @endif
            @if(hasPermission("list-chats", false))
            <li class="nav-item">
                <a class="nav-link" href="{{site()}}/panel/chats"><i class="fas fa-comments"></i> Chat</a>
            </li>
            @endif
            @if(hasPermission("list-anamneses", false))
            <li class="nav-item">
                <a class="nav-link" href="{{site()}}/panel/anamneses"><i class="fas fa-headset"></i> Anamneses</a>
            </li>
            @endif
            @if(hasPermission("list-medical-records", false))
            <li class="nav-item">
                <a class="nav-link" href="{{site()}}/panel/medical-records"><i class="fas fa-notes-medical"></i> Prontuários</a>
            </li>
            @endif
            @if(hasPermission("list-users", false))
            <li class="nav-item">
                <a class="nav-link" href="{{site()}}/panel/users"><i class="fas fa-user-friends"></i> Usuários</a>
            </li>
            @endif
            @if(hasPermission("list-permissions", false))
                <li class="nav-item">
                    <a class="nav-link" href="{{site()}}/panel/permissions"><i class="fas fa-lock"></i> Permissões</a>
                </li>
            @endif
        </ul>
    </div>
</nav>