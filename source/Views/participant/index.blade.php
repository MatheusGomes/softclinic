@extends('templates.panel')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-between">
            <h3 class="title">Lista de Participantes</h3>
            @if(hasPermission("create-participants", false))
            <a href="{{site()}}/panel/participants/register" class="btn btn-primary">Cadastrar Novo</a>
            @endif
        </div>
        <div class="card shadow mb-4">
            <div class="card-body">
                {!! getFlash() !!}
                <table class="table table-striped table-responsive-md" id="dataTable">
                    <thead class="table-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">CPF</th>
                        <th scope="col">Cidade</th>
                        <th scope="col">Telefone</th>
                        <th scope="col">Tipo</th>
                        @if(hasPermission("edit-participants", false) || hasPermission("delete-participants", false))
                        <th scope="col">Ação</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($participant as $participantItem)
                        <tr>
                            <th scope="row">{{ $participantItem->cdparticipante }}</th>
                            <td>{{ $participantItem->nmparticipante }}</td>
                            <td>{{ $participantItem->cpfparticipante }}</td>
                            <td>{{ $participantItem->nmcidade }}</td>
                            <td>{{ $participantItem->numcelular }}</td>
                            <td>{{ $participantItem->nmtipoparticipante }}</td>
                            <td class="text-center">
                                @if(hasPermission("edit-participants", false))
                                <a href="{{site()}}/panel/participants/edit/{{ $participantItem->cdparticipante }}" class="btn btn-default d-inline" data-toggle="tooltip" data-placement="top" title="Clique para editar"><i class="fas fa-pencil-alt"></i></a>
                                @endif
                                @if(hasPermission("delete-participants", false))
                                <a href="{{site()}}/panel/participants/delete/{{ $participantItem->cdparticipante }}" class="btn btn-danger d-inline j_delete" data-toggle="tooltip" data-placement="top" title="Clique para deletar"><i class="fas fa-trash-alt"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection