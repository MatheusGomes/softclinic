@extends('templates.panel')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">{{isset($participant) ? "Editar Participantes" : "Cadastro de Participantes"}}</h3>
                <a href="{{site()}}/panel/participants" class="btn btn-default">Voltar para pesquisa</a>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form action="" class="form-register" method="POST">
                        {!! getFlash() !!}
                        <div class="form-row">
                            <div class="form-group col-2">
                                <label for="typeParticipante">Tipo:</label>
                                <select name="participante" class="form-control select-single" id="typeParticipante" required>
                                    @foreach($typeParticipant as $typeParticipantItem)
                                        <option value="{{$typeParticipantItem->cdtipoparticipante}}" {{( isset($participant) && $participant->cdtipoparticipante == $typeParticipantItem->cdtipoparticipante) ? "selected" : ""}}>
                                            {{$typeParticipantItem->nmtipoparticipante}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="nome">Nome:</label>
                                <input type="text" id="nome" name="nome" class="form-control my-0" placeholder="Nome Completo" value="{{$participant->nmparticipante ?? ""}}" required />
                            </div>
                            <div class="form-group col">
                                <label for="email">Email:</label>
                                <input type="text" id="email" name="email" class="form-control my-0" placeholder="Email" value="{{$participant->email ?? ""}}" required />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="cfp">CPF:</label>
                                <input type="text" name="cpf" class="form-control" placeholder="CPF" size="14" value="{{$participant->cpfparticipante ?? ""}}" required>
                            </div>
                            <div class="form-group col">
                                <label for="telefone">Telefone:</label>
                                <input type="text" name="telefone" id="telefone" maxlength="15" class="form-control" placeholder="Telefone" value="{{$participant->numcelular ?? ""}}" required>
                            </div>
                            <div class="form-group col">
                                <label for="sexo">Sexo:</label>
                                <select name="sexo" class="form-control select-single" id="sexo" required>
                                    @foreach($sexParticipant as $listSexParticipantItem)
                                        <option value="{{$listSexParticipantItem->cdsexo}}" {{( isset($participant) && $participant->cdsexo == $listSexParticipantItem->cdsexo) ? "selected" : ""}}>
                                            {{$listSexParticipantItem->nmsexo}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="rua">Endereço:</label>
                                <input name="rua" type="text" id="rua" size="60" class="form-control" placeholder="Endereço" value="{{$participant->nmlogradouro ?? ""}}" required>
                            </div>
                            <div class="form-group col-2">
                                <label for="bairro">Bairro:</label>
                                <input name="bairro" type="text" id="bairro" size="40" class="form-control" placeholder="Bairro" value="{{$participant->nmbairro ?? ""}}" required>
                            </div>
                            <div class="form-group col-2">
                                <label for="cep">CEP:</label>
                                <input name="cep" type="text" id="cep" size="10" maxlength="9" class="form-control" placeholder="CEP" value="{{$participant->numcep ?? ""}}" required>
                            </div>
                            <div class="form-group col-2">
                                <label for="cidade">Cidade:</label>
                                <select name="cidade" class="form-control select-single" id="cidade" required>
                                    @foreach($city as $cityItem)
                                        <option value="{{$cityItem->cdcidade}}" {{( isset($participant) && $participant->cdcidade == $cityItem->cdcidade) ? "selected" : ""}}>
                                            {{$cityItem->nmcidade}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">{{isset($participant) ? "Salvar" : "Cadastrar"}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection