@extends('templates.panel')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">Grupo de Permissões</h3>
                @if(hasPermission("create-permissions", false))
                <a href="{{site()}}/panel/permissions/register" class="btn btn-primary">Cadastrar Novo</a>
                @endif
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    {!! getFlash() !!}
                    <table class="table dispĺay table-striped table-responsive-md" id="dataTable">
                        <thead class="table-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            @if(hasPermission("edit-permissions", false) || hasPermission("delete-permissions", false))
                            <th scope="col">Ações</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($groups as $group)
                            <tr>
                                <th scope="row">{{$group->cdgrupopermissao}}</th>
                                <td>{{$group->nmgrupopermissao}}</td>
                                <td class="text-center">
                                    @if(hasPermission("edit-permissions", false))
                                    <a href="{{site()}}/panel/permissions/edit/{{$group->cdgrupopermissao}}" class="btn btn-default d-inline" data-toggle="tooltip" data-placement="top" title="Clique para editar"><i class="fas fa-pencil-alt"></i></a>
                                    @endif
                                    @if(hasPermission("delete-permissions", false))
                                    <a href="{{site()}}/panel/permissions/delete/{{$group->cdgrupopermissao}}" class="btn btn-danger d-inline j_delete" data-toggle="tooltip" data-placement="top" title="Clique para deletar"><i class="fas fa-trash-alt"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection