@extends('templates.panel')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">{{isset($groupData) ? "Editar Grupo de Permissões" : "Cadastro de Grupo de Permissões"}}</h3>
                <a href="{{site()}}/panel/permissions" class="btn btn-default">Voltar para pesquisa</a>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form action="" class="form-register" method="POST">
                        {!! getFlash() !!}
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="nome">Nome do grupo:</label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="{{$groupData->nmgrupopermissao ?? ""}}"
                                       placeholder="Informe o nome do grupo" required autofocus/>
                            </div>
                            <div class="form-group col">
                                <label for="nome">Permissões:</label>
                                <select name="permissoes[]" id="permissoes" multiple
                                        class="form-control select-multiple" required>
                                    @foreach($permissions as $permissionItem)
                                        <option value="{{$permissionItem->cdpermissao}}" {{(isset($groupData) && in_array($permissionItem->cdpermissao, $hasPermissions)) ? "selected" : ""}}>
                                            {{$permissionItem->nmpermissao}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success">
                            {{isset($groupData) ? "Salvar" : "Cadastrar"}}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection