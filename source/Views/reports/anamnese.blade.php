<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anamnese - {{site("name")}}</title>
    <style>
        body {
            font-family: Arial, serif;
            font-size: 18px;
        }
        @page {
            margin: 70px 10px;
        }
        header {
            text-align: center;
            margin-top: -70px;
            padding: 10px 0;
            border-bottom: 1px solid #ddd;
        }
        main h1 {
            font-weight: normal;
            text-align: center;
        }
        main h1 span {
            font-weight: bold;
        }
        .participante, .animal {
            text-align: center;
            width: 100%;
        }
        .w-100 {
            width: 100%;
        }
        .informacoes td {
            text-align: left;
        }
        .text-left {

            text-align: left;
        }
        .mb-3 {
            margin-bottom: 30px;
        }
        .my-0 {
            margin-top: 0;
            margin-bottom: 0;
        }
    </style>
</head>
<body>
    <header>
        <img src="public/assets/img/logo-inline.png" alt="SoftClinic" />
    </header>
    <main>
        <h1>Anamnese</h1>
        <fieldset class="mb-3">
            <legend>Detalhes do Cliente</legend>
            <table class="participante w-100">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>Telefone</th>
                    <th>Endereço</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$participant->nmparticipante}}</td>
                    <td>{{$participant->cpfparticipante}}</td>
                    <td>{{$participant->numcelular}}</td>
                    <td>{{$participant->nmlogradouro}}, {{$participant->nmbairro}}</td>
                </tr>
                </tbody>
            </table>
        </fieldset>

        <fieldset class="mb-3">
            <legend>Detalhes do Animal</legend>
            <table class="animal w-100">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Sexo</th>
                    <th>Peso</th>
                    <th>Idade</th>
                    <th>Espécie</th>
                    <th>Raça</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$animal->nmanimal}}</td>
                    <td>{{($animal->sexo == "macho") ? "Macho" : "Fêmea"}}</td>
                    <td>{{$animal->peso}} kg</td>
                    <td>{{($animal->idade > 1) ? $animal->idade . " Meses" : $animal->idade . " Mês"}}</td>
                    <td>{{$animal->especie}}</td>
                    <td>{{$animal->raca}}</td>
                </tr>
                </tbody>
            </table>
        </fieldset>

        <fieldset class="mb-3">
            <legend>Informações Gerais</legend>
            <table class="informacoes w-100 mb-3">
                <tbody>
                    <tr>
                        <td><strong>Queixa principal:</strong> {{$anamnese->informacoes->queixaprincipal}}</td>
                    </tr>
                    <tr>
                        <td><strong>Tempo de evolução:</strong> {{$anamnese->informacoes->tempoevolucao}}</td>
                    </tr>
                    <tr>
                        <td><strong>Tipo de evolução:</strong> {{$anamnese->informacoes->tipoevolucao}}</td>
                    </tr>
                    <tr>
                        <td><strong>Tratamento:</strong> {{$anamnese->informacoes->tipotratamento}}</td>
                    </tr>
                </tbody>
            </table>
            <hr />
            <h4 class="my-0">Manejo Geral e História Pregressa</h4>
            <hr />
            <table class="informacoes w-100 mb-3">
                <tbody>
                    <tr>
                        <td><strong>Alimentação:</strong> {{$anamnese->informacoes->alimentacao}}</td>
                    </tr>
                    <tr>
                        <td><strong>Vacinação:</strong> {{$anamnese->informacoes->vacinacao}}</td>
                    </tr>
                    <tr>
                        <td><strong>Vermifugação:</strong> {{$anamnese->informacoes->vermifugacao}}</td>
                    </tr>
                    <tr>
                        <td><strong>Ambiente:</strong> {{$anamnese->informacoes->ambiente}}</td>
                    </tr>
                    <tr>
                        <td><strong>Acesso à rua? Fugiu recentemente?</strong> {{$anamnese->informacoes->acessorua}}</td>
                    </tr>
                    <tr>
                        <td><strong>Doenças anteriores:</strong> {{$anamnese->informacoes->doencasanteriores}}</td>
                    </tr>
                </tbody>
            </table>
        </fieldset>

        <fieldset class="mb-3">
            <legend>Informações do Sistema Digestório</legend>
            <table class="informacoes w-100 mb-3">
                <tbody>
                    <tr>
                        <td><strong>Ingestão de alimento:</strong>
                            {{
                                ($anamnese->informacoes->ingestaoalimento == "anorexia") ? "Anorexia (paciente não come nada)" :
                                (
                                    ($anamnese->informacoes->ingestaoalimento == "hiporexia") ? "Hiporexia (paciente come pouco)" :
                                    (
                                        ($anamnese->informacoes->ingestaoalimento == "normorexia") ? "Normorexia (paciente come normal)" : "Polifagia (paciente come mais que o habitual)"
                                    )
                                )
                            }}
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Ingestão de água:</strong>
                            {{
                                ($anamnese->informacoes->ingestaoagua == "adpsia") ? "Adpsia (paciente não ingere água)" :
                                (
                                    ($anamnese->informacoes->ingestaoagua == "hipodipsia") ? "Hipodipsia (paciente ingere menos água que o habitual)" :
                                    (
                                        ($anamnese->informacoes->ingestaoagua == "normodipsia") ? "Normodipsia (Ingestão normal de água)" : "Polidpsia (paciente ingere mais água que o habitual)"
                                    )
                                )
                            }}
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Defecação:</strong>
                            {{
                                ($anamnese->informacoes->defecacao == "normoquezia") ? "Normoquezia (Defeca normalmente)" :
                                (
                                    ($anamnese->informacoes->defecacao == "tenesmo") ? "Tenesmo (Dificuldade para defecar)" :
                                    (
                                        ($anamnese->informacoes->defecacao == "diarreia") ? "Diarréia (Fezes amolecidas)" :
                                        (
                                            ($anamnese->informacoes->defecacao == "melena") ? "Melena (Sangue digerido nas fezes - Remete a uma afecção de TGI superior)" : "Hematoquezia (Sangue digerido nas fezes - Remete a uma afecção de TGI superior)"
                                        )
                                    )
                                )
                             }}
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Abdômen:</strong> {{($anamnese->informacoes->abdomen == "abdominalgia") ? "Abdominalgia (Dor abdominal)" : "Organomegalia (Aumento de algum órgão à palpação)"}}</td>
                    </tr>
                    <tr>
                        <td><strong>Observações adicionais:</strong> {{$anamnese->informacoes->observacaodigestorio}}</td>
                    </tr>
                </tbody>
            </table>
        </fieldset>

        <fieldset class="mb-3">
            <legend>Informações do Cardiorrespiratório</legend>
            <table class="informacoes w-100 mb-3">
                <tbody>
                    <tr>
                        <td><strong>Apresenta cansaço fácil?</strong> {{$anamnese->informacoes->cansacofacil}}</td>
                    </tr>
                    <tr>
                        <td><strong>Apresentou algum episódio de cianose?</strong> {{$anamnese->informacoes->apresentacianose}}</td>
                    </tr>
                    <tr>
                        <td><strong>Tosse? Qual a frequência? É tosse seca ou com expectoração (produtiva)?</strong> {{$anamnese->informacoes->tosseanimal}}</td>
                    </tr>
                    <tr>
                        <td><strong>Qual o aspecto da expectoração (cor, odor, volume)?</strong> {{$anamnese->informacoes->tipoexpectoracao}}</td>
                    </tr>
                    <tr>
                        <td><strong>O animal tem espirros? Com que frequência?</strong> {{$anamnese->informacoes->apresentaespirros}}</td>
                    </tr>
                    <tr>
                        <td><strong>Secreção nasal? Serosa, purulenta, sanguinolenta (Epistaxe)</strong> {{$anamnese->informacoes->secrecaonasal}}</td>
                    </tr>
                    <tr>
                        <td><strong>Observou edema ou inchaço em alguma parte do corpo?</strong> {{$anamnese->informacoes->inchacoanimal}}</td>
                    </tr>
                    <tr>
                        <td><strong>O animal lhe parece fraco?</strong> {{$anamnese->informacoes->animalfraco}}</td>
                    </tr>
                </tbody>
            </table>
        </fieldset>

        <fieldset class="mb-3">
            <legend>Informações do Cardiorrespiratório</legend>
            <table class="informacoes w-100 mb-3">
                <tbody>
                <tr>
                    <td><strong>O animal está urinando?</strong> {{(isset($anamnese->informacoes->animalurinando) == "sim") ? "Sim" : "Não"}}</td>
                </tr>
                <tr>
                    <td><strong>Onde o animal urina aparecem formigas?</strong> {{(isset($anamnese->informacoes->urinacomformigas) == "sim") ? "Sim" : "Não"}}</td>
                </tr>
                <tr>
                    <td><strong>Qual a frequência?</strong> {{$anamnese->informacoes->frequenciaurina}}</td>
                </tr>
                <tr>
                    <td><strong>Qual a coloração da urina?</strong> {{$anamnese->informacoes->coloracaourina}}</td>
                </tr>
                <tr>
                    <td><strong>Qual o odor?</strong> {{$anamnese->informacoes->odorurina}}</td>
                </tr>
                <tr>
                    <td><strong>Aparentemente o animal sente dor quando urina?</strong> {{$anamnese->informacoes->dorurina}}</td>
                </tr>
                <tr>
                    <td><strong>Tipo de urina:</strong>
                        {{
                            ($anamnese->informacoes->urinacomformigas == "normouria") ? "Normoúria (paciente urina normalmente)" :
                            (
                                ($anamnese->informacoes->urinacomformigas == "anuria") ? "Anúria (paciente não produz urina)" :
                                (
                                    ($anamnese->informacoes->urinacomformigas == "oliguria") ? "Oligúria (paciente urina em pouca quantidade)" :
                                    (
                                        ($anamnese->informacoes->urinacomformigas == "poliuria") ? "Poliúria (paciente urina em grande quantidade)" :
                                        (
                                            ($anamnese->informacoes->urinacomformigas == "disuria") ? "Disúria (Dor ou ardor ao urinar)" : "Estrangúria (eliminação lenta e dolorosa de urina)"
                                        )
                                    )
                                )
                            )
                        }}
                    </td>
                </tr>
                </tbody>
            </table>
        </fieldset>

        <fieldset class="mb-3">
            <legend>Informações da Genital</legend>
            <table class="informacoes w-100 mb-3">
                <tbody>
                    <tr>
                        <td><strong>Castrado(a)?</strong> {{(isset($anamnese->informacoes->animalcastrado) == "sim") ? "Sim" : "Não"}}</td>
                    </tr>
                    @if($animal->sexo == "macho")
                    <tr>
                        <td><strong>Testículos:</strong> {{$anamnese->informacoes->testiculos}}</td>
                    </tr>
                    <tr>
                        <td><strong>Secreção peniana:</strong> {{$anamnese->informacoes->tosseanimal}}</td>
                    </tr>
                    <tr>
                        <td><strong>Presença massa ou sangramento peniano:</strong> {{$anamnese->informacoes->tipoexpectoracao}}</td>
                    </tr>
                    @else
                    <tr>
                        <td><strong>O animal já pariu alguma vez?</strong> {{$anamnese->informacoes->japariu}}</td>
                    </tr>
                    <tr>
                        <td><strong>Secreção vaginal:</strong> {{$anamnese->informacoes->secrecaovaginal}}</td>
                    </tr>
                    <tr>
                        <td><strong>Presença de cio:</strong> {{$anamnese->informacoes->presencacio}}</td>
                    </tr>
                    <tr>
                        <td><strong>Aplicação de anticoncepcional:</strong> {{$anamnese->informacoes->anticoncepcional}}</td>
                    </tr>
                    <tr>
                        <td><strong>Presença massa ou sangramento vaginal:</strong> {{$anamnese->informacoes->sangramentovaginal}}</td>
                    </tr>
                    <tr>
                        <td><strong>Nódulo mamário: </strong> {{$anamnese->informacoes->nodulomamario}}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </fieldset>

        <fieldset class="mb-3">
            <legend>Informações do Sistema Nervoso</legend>
            <table class="informacoes w-100 mb-3">
                <tbody>
                    <tr>
                        <td><strong>Apresentou mudanças de comportamento (agressividade)?</strong> {{$anamnese->informacoes->comportamento}}</td>
                    </tr>
                    <tr>
                        <td><strong>Apresentou convulsões?</strong> {{(isset($anamnese->informacoes->convulsoes) == "sim") ? "Sim" : "Não"}}</td>
                    </tr>
                    <tr>
                        <td><strong>Apresenta dificuldade para andar? Tem dificuldade para subir escadas?</strong> {{(isset($anamnese->informacoes->dificuldadesandar) == "sim") ? "Sim" : "Não"}}</td>
                    </tr>
                    <tr>
                        <td><strong>Anda em círculos?</strong> {{(isset($anamnese->informacoes->andaemcirculos) == "sim") ? "Sim" : "Não"}}</td>
                    </tr>
                    <tr>
                        <td><strong>Apresenta inclinação de cabeça?</strong> {{(isset($anamnese->informacoes->inclinacaocabeca) == "sim") ? "Sim" : "Não"}}</td>
                    </tr>
                    <tr>
                        <td><strong>Apresenta tropeços ou quedas quando caminha?</strong> {{(isset($anamnese->informacoes->apresentatropecos) == "sim") ? "Sim" : "Não"}}</td>
                    </tr>
                    <tr>
                        <td><strong>Ele atende quando chama?</strong> {{(isset($anamnese->informacoes->atendechamados) == "sim") ? "Sim" : "Não"}}</td>
                    </tr>
                    <tr>
                        <td><strong>Fica o tempo todo deitado?</strong> {{(isset($anamnese->informacoes->ficadeitado) == "sim") ? "Sim" : "Não"}}</td>
                    </tr>
                    <tr>
                        <td><strong>Responde à estímulos?</strong> {{(isset($anamnese->informacoes->respondeestimulos) == "sim") ? "Sim" : "Não"}}</td>
                    </tr>
                </tbody>
            </table>
        </fieldset>

        <fieldset class="mb-3">
            <legend>Informações do Sistema Esquelético</legend>
            <table class="informacoes w-100 mb-3">
                <tbody>
                <tr>
                    <td><strong>O animal está mancando?</strong> {{$anamnese->informacoes->animalmancando}}</td>
                </tr>
                <tr>
                    <td><strong>Teve algum histórico de trauma recente?</strong> {{$anamnese->informacoes->traumasrecentes}}</td>
                </tr>
                <tr>
                    <td><strong>Sente alguma dor ao andar ou quando manipula os membros?</strong> {{$anamnese->informacoes->sentedores}}</td>
                </tr>
                <tr>
                    <td><strong>Animal que não está andando:</strong> {{$anamnese->informacoes->animalandando}}</td>
                </tr>
                </tbody>
            </table>
        </fieldset>


        <fieldset class="mb-3">
            <legend>Informações Dermatológica</legend>
            <table class="informacoes w-100 mb-3">
                <tbody>
                <tr>
                    <td><strong>O animal se coça muito?</strong> {{$anamnese->informacoes->animalseconcando}}</td>
                </tr>
                <tr>
                    <td><strong>Apresenta meneios de cabeça ou bate a pata na orelha o tempo todo (otite)?</strong> {{$anamnese->informacoes->maneiosdecabeca}}</td>
                </tr>
                <tr>
                    <td><strong>Queda de pêlos?</strong> {{$anamnese->informacoes->quedadepelos}}</td>
                </tr>
                <tr>
                    <td><strong>Tem ou teve carrapatos e pulgas recentemente? (Usou alguma medicação?)</strong> {{$anamnese->informacoes->tevecarrapatos}}</td>
                </tr>
                </tbody>
            </table>
        </fieldset>
    </main>
</body>
</html>