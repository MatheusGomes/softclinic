<!DOCTYPE html>
<html lang="{{site("locale")}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{site("name")}}</title>
    <meta name="description" content="{{site("desc")}}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" />
    <link rel="stylesheet" href="{{asset("css/bootstrap/bootstrap.min.css")}}" />
    <link rel="stylesheet" href="{{asset("css/fontawesome/fontawesome.min.css")}}" />
    <link rel="stylesheet" href="{{asset("css/select2/select2.min.css")}}" />
    <link rel="stylesheet" href="{{asset("css/style.css")}}" />
</head>
    <body data-url="{{site()}}">
        @yield('content')

        <script src="{{asset("js/jquery/jquery.min.js")}}"></script>
        <script src="{{asset("js/bootstrap/bootstrap.bundle.min.js")}}"></script>
        <script src="{{asset("js/select2/select2.min.js")}}"></script>
        <script src="{{asset("js/custom.js")}}"></script>
    </body>
</html>