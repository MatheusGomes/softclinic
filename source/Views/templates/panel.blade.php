<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{site("name")}}</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" />
    <link rel="stylesheet" href="{{asset("css/bootstrap/bootstrap.min.css")}}" />
    <link rel="stylesheet" href="{{asset("css/fontawesome/fontawesome.min.css")}}" />
    <link rel="stylesheet" href="{{asset("css/select2/select2.min.css")}}" />
    <link rel="stylesheet" href="{{asset("css/dataTables/dataTables.bootstrap4.min.css")}}" />
    <link rel="stylesheet" href="{{asset("css/style.css")}}" />
</head>
    <body data-url="{{site()}}">
        <div class="container-fluid dashboard">
            <div class="row justify-content-between dash-navbar-top">
                <div class="col text-left">
                    <img src="{{asset("img/logo-inline.png")}}" alt="SoftClinic" />
                </div>
                <div class="col text-right">
                    <ul>
{{--                        <li class="nav-item dropdown no-arrow">--}}
{{--                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                <i class="far fa-bell"></i>--}}
{{--                                <!-- Counter - Alerts -->--}}
{{--                                <span class="badge badge-danger badge-counter">3+</span>--}}
{{--                            </a>--}}
{{--                            <!-- Dropdown - Alerts -->--}}
{{--                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">--}}
{{--                                <h6 class="dropdown-header">--}}
{{--                                    Centro de notificações--}}
{{--                                </h6>--}}
{{--                                <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                    <div class="mr-3">--}}
{{--                                        <div class="icon-circle bg-primary">--}}
{{--                                            <i class="fas fa-file-alt text-white"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div>--}}
{{--                                        <div class="small text-gray-500">7 de Abril de 2020</div>--}}
{{--                                        <span class="font-weight-bold">Uma nova notificação!</span>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                                <a class="dropdown-item text-center small text-gray-500" href="#">Ver todas as notificações</a>--}}
{{--                            </div>--}}
{{--                        </li>--}}
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{\Source\Core\Session::get("auth")["nome"]}}
                            </a>
                            <!-- Dropdown - Alerts -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                                <a class="dropdown-item d-flex align-items-center" href="{{site()}}/panel/users/edit/{{\Source\Core\Session::get("auth")["cdusuario"]}}">
                                    Editar conta
                                </a>
                            </div>
                        </li>
                        <li><a href="{{site()}}/logout" class="text-danger"><i class="fas fa-sign-out-alt"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-2 dash-navbar-aside">
                    @include('includes.panel-sidebar')
                </div>
                <div class="col-10 dash-content mt-4">
                    @yield('content')
                </div>
            </div>
        </div>

        <script src="{{asset("js/jquery/jquery.min.js")}}"></script>
        <script src="{{asset("js/jquery/jquery.forn.min.js")}}"></script>
        <script src="{{asset("js/bootstrap/bootstrap.bundle.min.js")}}"></script>
        <script src="{{asset("js/datatables/jquery.dataTables.min.js")}}"></script>
        <script src="{{asset("js/datatables/dataTables.bootstrap4.min.js")}}"></script>
        <script src="{{asset("js/datatables/datatables-demo.js")}}"></script>
        <script src="{{asset("js/select2/select2.min.js")}}"></script>
        <script src="{{asset("js/chart/chart.min.js")}}"></script>
        <script src="{{asset("js/chart/chart-area-demo.js")}}"></script>
        <script src="{{asset("js/chart/chart-pie-demo.js")}}"></script>
        <script src="{{asset("js/custom.js")}}"></script>
    </body>
</html>