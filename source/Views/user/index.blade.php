@extends('templates.panel')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">Lista de Usuários</h3>
                @if(hasPermission("create-users", false))
                <a href="{{site()}}/panel/users/register" class="btn btn-primary">Cadastrar Novo</a>
                @endif
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    {!! getFlash() !!}
                    <table class="table dispĺay table-striped table-responsive-md" id="dataTable">
                        <thead class="table-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Data de cadastro</th>
                            @if(hasPermission("edit-users", false) || hasPermission("delete-users", false))
                            <th scope="col">Ações</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <th scope="row">{{$user->cdusuario}}</th>
                                <td>{{$user->emailusuario}}</td>
                                <td class="text-center small">{{$user->nmgrupopermissao}}
                                <td>{{timestampsToBR($user->dtcadastro)}}</td>
                                <td class="text-center">
                                    @if(hasPermission("edit-users", false))
                                    <a href="{{site()}}/panel/users/edit/{{$user->cdusuario}}" class="btn btn-default d-inline" data-toggle="tooltip" data-placement="top" title="Clique para editar"><i class="fas fa-pencil-alt"></i></a>
                                    @endif
                                    @if(hasPermission("delete-users", false))
                                    <a href="{{site()}}/panel/users/delete/{{$user->cdusuario}}" class="btn btn-danger d-inline j_delete" data-toggle="tooltip" data-placement="top" title="Clique para deletar"><i class="fas fa-trash-alt"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection