@extends('templates.panel')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">{{isset($user) ? "Editar Usuário" : "Cadastro de Usuário"}}</h3>
                @if(hasPermission("list-permissions", false))
                <a href="{{site()}}/panel/users" class="btn btn-default">Voltar para pesquisa</a>
                @endif
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form action="" class="form-register" method="POST">
                        {!! getFlash() !!}
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="email">E-mail</label>
                                <input type="email" name="email" id="email" class="form-control" value="{{$user->emailusuario ?? ""}}" placeholder="Endereço de e-mail" required />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col col-md-6">
                                <label for="senha">Senha</label>
                                <input type="password" name="senha" id="senha" class="form-control" placeholder="Senha" {{!isset($user) ? "required" : ""}} />
                            </div>
                            <div class="form-group col col-md-6">
                                <label for="confirmaSenha">Confirmar senha</label>
                                <input type="password" name="confirmaSenha" id="confirmaSenha" class="form-control" placeholder="Confirmar Senha" {{!isset($user) ? "required" : ""}} />
                            </div>
                        </div>

                        @if(hasPermission("list-permissions", false))
                        <div class="form-row">
                            <div class="form-group">
                                <label for="grupo">Grupo</label>
                                <select name="grupo" class="form-control" id="grupo" required>
                                    <option readonly disabled selected>Selecione</option>
                                    @foreach($groups as $groupItem)
                                        <option value="{{$groupItem->cdgrupopermissao}}" {{(isset($user) && $groupItem->cdgrupopermissao == $user->cdgrupopermissao) ? "selected" : ""}}>
                                            {{$groupItem->nmgrupopermissao}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif

                        <button type="submit" class="btn btn-success">{{isset($user) ? "Salvar" : "Cadastrar"}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection