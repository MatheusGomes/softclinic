
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-between">
            <h3 class="title">Lista de Participantes</h3>
            <a href="<?php echo e(site()); ?>/panel/participants/register" class="btn btn-primary">Cadastrar Novo</a>
        </div>
        <div class="card shadow mb-4">
            <div class="card-body">
                <?php echo getFlash(); ?>

                <table class="table table-striped table-responsive-md" id="dataTable">
                    <thead class="table-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">CPF</th>
                        <th scope="col">Cidade</th>
                        <th scope="col">Telefone</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $participant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $participantItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <th scope="row"><?php echo e($participantItem->cdparticipante); ?></th>
                            <td><?php echo e($participantItem->nmparticipante); ?></td>
                            <td><?php echo e($participantItem->cpfparticipante); ?></td>
                            <td><?php echo e($participantItem->nmcidade); ?></td>
                            <td><?php echo e($participantItem->numcelular); ?></td>
                            <td><?php echo e($participantItem->nmtipoparticipante); ?></td>
                            <td class="text-center">
                                <a href="<?php echo e(site()); ?>/panel/participants/edit/<?php echo e($participantItem->cdparticipante); ?>" class="btn btn-default d-inline"><i class="fas fa-pencil-alt"></i></a>
                                <a href="<?php echo e(site()); ?>/panel/participants/delete/<?php echo e($participantItem->cdparticipante); ?>" class="btn btn-danger d-inline"><i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('templates.panel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\softclinic\source\Views/participant/index.blade.php ENDPATH**/ ?>