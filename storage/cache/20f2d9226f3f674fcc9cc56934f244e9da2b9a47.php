
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title"><?php echo e(isset($participant) ? "Editar Participantes" : "Cadastro de Participantes"); ?></h3>
                <a href="<?php echo e(site()); ?>/panel/participants" class="btn btn-default">Voltar para pesquisa</a>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form action="" class="form-register" method="POST">
                        <?php echo getFlash(); ?>

                        <div class="form-row">
                            <div class="form-group col-2">
                                <label for="typeParticipante">Tipo:</label>
                                <select name="participante" class="form-control select-single" id="typeParticipante" required>
                                    <?php $__currentLoopData = $typeParticipant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeParticipantItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($typeParticipantItem->cdtipoparticipante); ?>" <?php echo e(( isset($participant) && $participant->cdtipoparticipante == $typeParticipantItem->cdtipoparticipante) ? "selected" : ""); ?>>
                                            <?php echo e($typeParticipantItem->nmtipoparticipante); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="nome">Nome:</label>
                                <input type="text" id="nome" name="nome" class="form-control my-0" placeholder="Nome Completo" value="<?php echo e($participant->nmparticipante ?? ""); ?>" required />
                            </div>
                            <div class="form-group col">
                                <label for="email">Email:</label>
                                <input type="text" id="email" name="email" class="form-control my-0" placeholder="Email" value="<?php echo e($participant->email ?? ""); ?>" required />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="cfp">CPF:</label>
                                <input type="text" name="cpf" class="form-control" placeholder="CPF" size="14" value="<?php echo e($participant->cpfparticipante ?? ""); ?>" required>
                            </div>
                            <div class="form-group col">
                                <label for="telefone">Telefone:</label>
                                <input type="text" name="telefone" id="telefone" maxlength="15" class="form-control" placeholder="Telefone" value="<?php echo e($participant->numcelular ?? ""); ?>" required>
                            </div>
                            <div class="form-group col">
                                <label for="sexo">Sexo:</label>
                                <select name="sexo" class="form-control select-single" id="sexo" required>
                                    <?php $__currentLoopData = $sexParticipant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $listSexParticipantItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($listSexParticipantItem->cdsexo); ?>" <?php echo e(( isset($participant) && $participant->cdsexo == $listSexParticipantItem->cdsexo) ? "selected" : ""); ?>>
                                            <?php echo e($listSexParticipantItem->nmsexo); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="rua">Endereço:</label>
                                <input name="rua" type="text" id="rua" size="60" class="form-control" placeholder="Endereço" value="<?php echo e($participant->nmlogradouro ?? ""); ?>" required>
                            </div>
                            <div class="form-group col-2">
                                <label for="bairro">Bairro:</label>
                                <input name="bairro" type="text" id="bairro" size="40" class="form-control" placeholder="Bairro" value="<?php echo e($participant->nmbairro ?? ""); ?>" required>
                            </div>
                            <div class="form-group col-2">
                                <label for="cep">CEP:</label>
                                <input name="cep" type="text" id="cep" size="10" maxlength="9" class="form-control" placeholder="CEP" value="<?php echo e($participant->numcep ?? ""); ?>" required>
                            </div>
                            <div class="form-group col-2">
                                <label for="cidade">Cidade:</label>
                                <select name="cidade" class="form-control select-single" id="cidade" required>
                                    <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cityItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($cityItem->cdcidade); ?>" <?php echo e(( isset($participant) && $participant->cdcidade == $cityItem->cdcidade) ? "selected" : ""); ?>>
                                            <?php echo e($cityItem->nmcidade); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success"><?php echo e(isset($participant) ? "Salvar" : "Cadastrar"); ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('templates.panel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\softclinic\source\Views/participant/register.blade.php ENDPATH**/ ?>