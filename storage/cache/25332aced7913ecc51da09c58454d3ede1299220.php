<!DOCTYPE html>
<html lang="<?php echo e(site("locale")); ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo e(site("name")); ?></title>
    <meta name="description" content="<?php echo e(site("desc")); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset("css/bootstrap/bootstrap.min.css")); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset("css/fontawesome/fontawesome.min.css")); ?>" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" />
    <link rel="stylesheet" href="<?php echo e(asset("css/style.css")); ?>" />
</head>
    <body data-url="<?php echo e(site()); ?>">
        <?php echo $__env->yieldContent('content'); ?>

        <script src="<?php echo e(asset("js/jquery/jquery.min.js")); ?>"></script>
        <script src="<?php echo e(asset("js/bootstrap/bootstrap.bundle.js")); ?>"></script>
    </body>
</html><?php /**PATH C:\xampp\htdocs\softclinic\source\Views/templates/auth.blade.php ENDPATH**/ ?>