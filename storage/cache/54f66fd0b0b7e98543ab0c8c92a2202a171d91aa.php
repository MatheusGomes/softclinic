
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title">Lista de Usuários</h3>
                <a href="<?php echo e(site()); ?>/panel/users/register" class="btn btn-primary">Cadastrar Novo</a>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    <?php echo getFlash(); ?>

                    <table class="table dispĺay table-striped table-responsive-md" id="dataTable">
                        <thead class="table-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Data de cadastro</th>
                            <th scope="col">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <th scope="row"><?php echo e($user->cdusuario); ?></th>
                                <td><?php echo e($user->emailusuario); ?></td>
                                <td class="text-center small"><?php echo e($user->nmgrupopermissao); ?>

                                <td><?php echo e($user->dtcadastro); ?></td>
                                <td class="text-center">
                                    <a href="<?php echo e(site()); ?>/panel/users/edit/<?php echo e($user->cdusuario); ?>" class="btn btn-default d-inline"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="<?php echo e(site()); ?>/panel/users/delete/<?php echo e($user->cdusuario); ?>" class="btn btn-danger d-inline"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('templates.panel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\softclinic\source\Views/user/index.blade.php ENDPATH**/ ?>