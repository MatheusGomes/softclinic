
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h3 class="title"><?php echo e(isset($user) ? "Editar Usuário" : "Cadastro de Usuário"); ?></h3>
                <a href="<?php echo e(site()); ?>/panel/users" class="btn btn-default">Voltar para pesquisa</a>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form action="" class="form-register" method="POST">
                        <?php echo getFlash(); ?>

                        <div class="form-row">
                            <div class="form-group col">
                                <label for="email">E-mail</label>
                                <input type="email" name="email" id="email" class="form-control" value="<?php echo e($user->emailusuario ?? ""); ?>" placeholder="Endereço de e-mail" required />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col col-md-6">
                                <label for="senha">Senha</label>
                                <input type="password" name="senha" id="senha" class="form-control" placeholder="Senha" <?php echo e(!isset($user) ? "required" : ""); ?> />
                            </div>
                            <div class="form-group col col-md-6">
                                <label for="confirmaSenha">Confirmar senha</label>
                                <input type="password" name="confirmaSenha" id="confirmaSenha" class="form-control" placeholder="Confirmar Senha" <?php echo e(!isset($user) ? "required" : ""); ?> />
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group">
                                <label for="grupo">Grupo</label>
                                <select name="grupo" class="form-control" id="grupo" required>
                                    <option readonly disabled selected>Selecione</option>
                                    <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $groupItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($groupItem->cdgrupopermissao); ?>" <?php echo e((isset($user) && $groupItem->cdgrupopermissao == $user->cdgrupopermissao) ? "selected" : ""); ?>>
                                            <?php echo e($groupItem->nmgrupopermissao); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success"><?php echo e(isset($user) ? "Salvar" : "Cadastrar"); ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('templates.panel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\softclinic\source\Views/user/register.blade.php ENDPATH**/ ?>