<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo e(site("name")); ?></title>
    <link rel="stylesheet" href="<?php echo e(asset("css/bootstrap/bootstrap.min.css")); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset("css/fontawesome/fontawesome.min.css")); ?>" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" />
    <link rel="stylesheet" href="<?php echo e(asset("css/select2/select2.min.css")); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset("css/dataTables/dataTables.bootstrap4.min.css")); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset("css/style.css")); ?>" />
</head>
    <body data-url="<?php echo e(site()); ?>">
        <div class="container-fluid dashboard">
            <div class="row justify-content-between dash-navbar-top">
                <div class="col text-left">
                    <img src="<?php echo e(asset("img/logo-inline.png")); ?>" alt="SoftClinic" />
                </div>
                <div class="col text-right">
                    <ul>

























                        <li><a href="<?php echo e(site()); ?>/logout" class="text-danger"><i class="fas fa-sign-out-alt"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-2 dash-navbar-aside">
                    <?php echo $__env->make('includes.panel-sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="col-10 dash-content mt-4">
                    <?php echo $__env->yieldContent('content'); ?>
                </div>
            </div>
        </div>

        <script src="<?php echo e(asset("js/jquery/jquery.min.js")); ?>"></script>
        <script src="<?php echo e(asset("js/bootstrap/bootstrap.bundle.min.js")); ?>"></script>
        <script src="<?php echo e(asset("js/dataTables/jquery.dataTables.min.js")); ?>"></script>
        <script src="<?php echo e(asset("js/dataTables.bootstrap4.min.js")); ?>"></script>
        <script src="<?php echo e(asset("js/select2/select2.min.js")); ?>"></script>
        <script src="<?php echo e(asset("js/custom.js")); ?>"></script>
    </body>
</html><?php /**PATH C:\xampp\htdocs\softclinic\source\Views/templates/panel.blade.php ENDPATH**/ ?>