<nav class="navbar navbar-expand-lg">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarAside" aria-controls="navbarAside" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarAside">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo e(site()); ?>/panel"><i class="fas fa-chart-line"></i> Dashboard <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(site()); ?>/panel/participants"><i class="fas fa-user-tie"></i> Participantes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(site()); ?>/panel/calls"><i class="fas fa-headset"></i> Atendimentos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(site()); ?>/panel/medical-records"><i class="fas fa-notes-medical"></i> Prontuários</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(site()); ?>/panel/users"><i class="fas fa-user-friends"></i> Usuários</a>
            </li>
        </ul>
    </div>
</nav><?php /**PATH C:\xampp\htdocs\softclinic\source\Views/includes/panel-sidebar.blade.php ENDPATH**/ ?>