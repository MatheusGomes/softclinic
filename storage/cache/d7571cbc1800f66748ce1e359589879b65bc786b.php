<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col justify-content-center text-center">
            <h5 class="d-flex justify-content-start mt-3">
                <a class="text-light btn bg-dark text-decoration-none" href="<?php echo e(site()); ?>/"> 
                    <img src="<?php echo e(asset("img/return-white.png")); ?>" alt="<"/> Início
                </a>
            </h5>
            <div class="col erro400">
                <img class="img-fluid my-5 img2 col-md-5" src="<?php echo e(asset("img/errors/400.png")); ?>" alt="erro400" />
                <img class="img-fluid my-5 img1 col-md-5" src="<?php echo e(asset("img/errors/gato.png")); ?>" alt="erro400" />
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('templates.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\softclinic\source\Views/errors/400.blade.php ENDPATH**/ ?>