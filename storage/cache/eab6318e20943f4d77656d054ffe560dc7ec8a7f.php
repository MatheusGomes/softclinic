<header>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>
        if ($("<input />").prop("required") === undefined) {
        $(document).on("submit", function(e) {
            $(this)
            .find("input, select, textarea")
            .filter("[required]")
            .filter(function() { return this.value == ''; })
            .each(function() {
                e.preventDefault();
                $(this).css({ "border": "2px solid red" })
                alert($(this).prev('label').html() + " é obrigatório.");
            });
        });
        }
    </script>
</header>



<?php $__env->startSection('content'); ?>
<div class="container-fluid login">
    <div class="row align-items-center">
        <div class="col-12 col-md-6 order-2 order-md-1">
            <div class="logo text-center mb-5">
                <img src="<?php echo e(asset("img/logo.png")); ?>" alt="Sofclin" />
            </div>
            <div class="container shadow col col-md-12 col-lg-10">
                <div class="row">
                    <div class="col my-3">
                        <a class="text-dark text-decoration-none" href="<?php echo e(site()); ?>/"> <img src="<?php echo e(asset("img/return.png")); ?>" alt="<"/> Login</a>
                    </div>
                </div>
                <div class="col text-center">
                    <h4>Recuperação de senha</h4>
                </div>
                <form class="form-row col-10 mx-auto mt-3" method="POST" action="<?php echo e(site()); ?>/redirect-recovery">
                    <div class="input-group mb-5">
                        <input type="email" id="email" class="form-control" required placeholder="Informe o seu e-mail" aria-label="E-mail" aria-describedby="button-submit">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary bg-primary"  type="submit" id="button-submit">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
            <hr class=" col col-md-9 mt-5">
            <div class="row text-center">
                <div class="col">
                    Não possui cadastro? &nbsp;<a class="font-italic text-secondary font-weight-bold text-decoration-none" href="<?php echo e(site()); ?>/register_client">Criar conta</a>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 order-1  order-md-2 background-login text-center">
            <h2>RECUPERAÇÃO DE SENHA</h2>
            <h5> Soft<span>Clinc</span>!</h5>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('templates.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\softclinic\source\Views/auth/recovery.blade.php ENDPATH**/ ?>